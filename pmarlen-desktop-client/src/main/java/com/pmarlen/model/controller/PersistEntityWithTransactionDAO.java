/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.model.controller;

import com.pmarlen.client.ProgressProcessListener;
import com.pmarlen.model.beans.*;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.DecimalFormat;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author alfred
 */
@Repository("persistEntityWithTransactionDAO")
public class PersistEntityWithTransactionDAO {

	private Logger logger;
	private EntityManagerFactory emf;

	@Autowired
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public PersistEntityWithTransactionDAO() {
		logger = LoggerFactory.getLogger(PersistEntityWithTransactionDAO.class);
	}

	public Cliente persistCliente(Cliente c) throws Exception {
		logger.debug("->persistCliente: ");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			em.getTransaction().begin();
			logger.debug("->Begin transaction");

			Cliente clienteToPersist = new Cliente();

			clienteToPersist.setRfc(c.getRfc());
			clienteToPersist.setFechaCreacion(c.getFechaCreacion());
			clienteToPersist.setRazonSocial(c.getRazonSocial());
			clienteToPersist.setNombreEstablecimiento(c.getNombreEstablecimiento());
			clienteToPersist.setCalle(c.getCalle());
			clienteToPersist.setNumInterior(c.getNumInterior());
			clienteToPersist.setNumExterior(c.getNumExterior());
			clienteToPersist.setPoblacion(c.getPoblacion());
			clienteToPersist.setTelefonos(c.getTelefonos());
			clienteToPersist.setEmail(c.getEmail());
			clienteToPersist.setUrl(c.getUrl());
			clienteToPersist.setObservaciones(c.getObservaciones());

			em.persist(clienteToPersist);

			em.getTransaction().commit();
			logger.debug("->Commit");

			return clienteToPersist;
		} catch (Exception e) {
			logger.error("Exception caught:", e);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw e;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void create(Cliente cliente) {
		if (cliente.getPedidoVentaCollection() == null) {
			cliente.setPedidoVentaCollection(new ArrayList<PedidoVenta>());
		}
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Poblacion poblacion = cliente.getPoblacion();
			if (poblacion != null) {
				poblacion = em.getReference(poblacion.getClass(), poblacion.getId());
				cliente.setPoblacion(poblacion);
			}

			Collection<PedidoVenta> attachedPedidoVentaCollection = new ArrayList<PedidoVenta>();
			for (PedidoVenta pedidoVentaCollectionPedidoVentaToAttach : cliente.getPedidoVentaCollection()) {
				pedidoVentaCollectionPedidoVentaToAttach = em.getReference(pedidoVentaCollectionPedidoVentaToAttach.getClass(), pedidoVentaCollectionPedidoVentaToAttach.getId());
				attachedPedidoVentaCollection.add(pedidoVentaCollectionPedidoVentaToAttach);
			}
			cliente.setPedidoVentaCollection(attachedPedidoVentaCollection);
			em.persist(cliente);
			if (poblacion != null) {
				poblacion.getClienteCollection().add(cliente);
				poblacion = em.merge(poblacion);
			}

			for (PedidoVenta pedidoVentaCollectionPedidoVenta : cliente.getPedidoVentaCollection()) {
					Cliente oldClienteOfPedidoVentaCollectionPedidoVenta = pedidoVentaCollectionPedidoVenta.getCliente();
				pedidoVentaCollectionPedidoVenta.setCliente(cliente);
				pedidoVentaCollectionPedidoVenta = em.merge(pedidoVentaCollectionPedidoVenta);
				if (oldClienteOfPedidoVentaCollectionPedidoVenta != null) {
					oldClienteOfPedidoVentaCollectionPedidoVenta.getPedidoVentaCollection().remove(pedidoVentaCollectionPedidoVenta);
					oldClienteOfPedidoVentaCollectionPedidoVenta = em.merge(oldClienteOfPedidoVentaCollectionPedidoVenta);
				}
			}
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public Cliente updateCliente(Cliente c) throws Exception {
		logger.debug("->persistCliente: ");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			em.getTransaction().begin();
			logger.debug("->Begin transaction");

			Cliente xc = em.getReference(Cliente.class, c.getId());

			copySimpleProperties(xc, c);

			em.getTransaction().commit();
			logger.debug("->Commit");

			logger.debug("->Ok, Cliente created:" + c.getId());

			return c;
		} catch (Exception e) {
			logger.error("Exception caught:", e);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw e;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void deleteCliente(Cliente del) throws Exception {
		Cliente x = null;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			em.getTransaction().begin();
			logger.debug("->begin transaction");
			x = em.find(del.getClass(), del.getId());
			logger.debug("->ok, Cliente retrieved");
			em.remove(x);
			logger.debug("->ok, Cliente removed");
			em.getTransaction().commit();
			logger.debug("->Ok, commit");
		} catch (RollbackException rbe) {
			logger.error("->Some has wrong. shuld rollback !", rbe);
			em.getTransaction().rollback();
			throw rbe;
		} catch (Exception e) {
			logger.error("->Some has wrong", e);
			throw e;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void deleteAllObjects() throws Exception {
		logger.debug("->deleteAllObjects: ");
		String qry = null;
		Query q = null;
		int r = -1;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			em.getTransaction().begin();

			Class entityClassNames[] = {
				PedidoVentaEstado.class, PedidoVentaDetalle.class,
				PedidoVenta.class};

			Class entityRelationClassNames[] = {
				Perfil.class, Usuario.class, Cliente.class,
				Multimedio.class, Producto.class, Marca.class,
				Linea.class, Industria.class, Estado.class, FormaDePago.class, Almacen.class, Sucursal.class};
			
			logger.debug("------------->Start to delete the common entityes.");
			for (Class entityClass : entityClassNames) {

				qry = "delete from " + entityClass.getSimpleName() + " x where 1=1 ";
				q = em.createQuery(qry);
				r = q.executeUpdate();
				em.flush();
				logger.debug("->flusshed, result delete " + entityClass.getSimpleName() + " : " + r);
			}
			logger.debug("------------->END COMMON ENTITYES, Start to delete the relations.");
			for (Class entityRelatedClass : entityRelationClassNames) {

				q = em.createQuery("select x from " + entityRelatedClass.getSimpleName() + " x ");
				List listEntities = q.getResultList();

				logger.debug("\t->remove " + entityRelatedClass.getSimpleName() + ", for the list:" + listEntities);
				for (Object objEntity : listEntities) {
					logger.debug("\t\t->Ok remove Related entitie:" + objEntity.getClass().getSimpleName());
					em.remove(objEntity);
				}
				em.flush();
				logger.debug("\t->Ok remove all Releated entities.");
			}

			logger.debug("Prepared to exec commint !.");

			em.getTransaction().commit();

			logger.debug("->commit delleteAllObjects!!");
		} catch (Exception ex) {
			logger.error("-------->>> delleteAllObjects:" + ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void deletePedidos() throws Exception {
		logger.debug("->deletePedidos: ");
		String qry = null;
		Query q = null;
		int r = -1;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			em.getTransaction().begin();

			Class entityRelationClassNames[] = {PedidoVentaEstado.class, PedidoVentaDetalle.class, PedidoVenta.class};

			for (Class entityRelatedClass : entityRelationClassNames) {

				q = em.createQuery("select x from " + entityRelatedClass.getSimpleName() + " x ");
				List listEntities = q.getResultList();

				logger.debug("\t->remove " + entityRelatedClass.getSimpleName() + " the lis:" + listEntities);
				for (Object objEntity : listEntities) {
					logger.debug("\t\t->Ok remove Related entitie:" + objEntity.getClass().getSimpleName());
					em.remove(objEntity);
				}
				em.flush();
				logger.debug("\t->Ok remove all Releated entities.");
			}

			em.getTransaction().commit();

			logger.debug("->commit delletePedidos!!");
		} catch (Exception ex) {
			//logger.error( "delletePedidos:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void inicializarUsuarios(List<Usuario> usuarioReceivedList, ProgressProcessListener progressListener) throws Exception {
		logger.debug("->inicializarUsuarios: ");

		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for Usuarios");
			em.getTransaction().begin();
			int total = usuarioReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;

			Hashtable<Perfil, HashSet<Usuario>> usuarioDePerfiles = new Hashtable<Perfil, HashSet<Usuario>>();

			for (Usuario usr : usuarioReceivedList) {
				logger.debug("\t->Usuario to insert:"+usr.getUsuarioId());
				Usuario usuarioNuevo = new Usuario();

				usuarioNuevo.setUsuarioId(usr.getUsuarioId());
				usuarioNuevo.setNombreCompleto(usr.getNombreCompleto());
				usuarioNuevo.setPassword(usr.getPassword());
				usuarioNuevo.setEmail(usr.getEmail());
				usuarioNuevo.setHabilitado(usr.getHabilitado());
				usuarioNuevo.setSucursal(usr.getSucursal());
				usuarioNuevo.setPerfilCollection(null);

				em.persist(usuarioNuevo);
				Collection<Perfil> perfilListInUsuario = usr.getPerfilCollection();
				logger.debug("\t->perfilListInUsuario:"+perfilListInUsuario);
				i++;
				progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),"actualizando Usuario :" + formatProgress(i, total));
				if (perfilListInUsuario != null) {
					for (Perfil perfil : perfilListInUsuario) {
						logger.debug("\t\t->+Perfil:"+perfil.getId());
						HashSet<Usuario> usuarios = usuarioDePerfiles.get(perfil);
						if (usuarios == null) {
							usuarios = new HashSet<Usuario>();
						}
						usuarios.add(usuarioNuevo);
						usuarioDePerfiles.put(perfil, usuarios);
					}
				}
			}
			em.flush();

			Enumeration<Perfil> enumPerfiles = usuarioDePerfiles.keys();

			while (enumPerfiles.hasMoreElements()) {

				Perfil perfilASincronizar = enumPerfiles.nextElement();
				Perfil perfil = null;

				perfil = new Perfil(perfilASincronizar.getId());
				perfil.setDescripcion(perfilASincronizar.getDescripcion());
				em.persist(perfil);
			}
			em.flush();

			enumPerfiles = usuarioDePerfiles.keys();

			while (enumPerfiles.hasMoreElements()) {
				Perfil perfil = enumPerfiles.nextElement();
				HashSet<Usuario> usuarios = usuarioDePerfiles.get(perfil);

				Perfil perfilASincronizar = em.find(Perfil.class, perfil.getId());

				perfilASincronizar.setUsuarioCollection(usuarios);
				em.merge(perfilASincronizar);
			}
			em.flush();

			em.getTransaction().commit();
			logger.debug("->commit the collection for Usuarios OK!!");
		} catch (Exception ex) {
			//logger.error( "inicializarUsuarios:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void inicializarLinea(List<Linea> lineaReceivedList, ProgressProcessListener progressListener) throws Exception {
		logger.debug("->inicializarLineas: V5");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for Linea");
			em.getTransaction().begin();
			int total = lineaReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;
			for (Linea linea : lineaReceivedList) {
				Query nativeQuery = em.createNativeQuery("INSERT INTO LINEA(ID,NOMBRE) VALUES (?,?)");
				nativeQuery.setParameter(1, linea.getId());
				nativeQuery.setParameter(2, linea.getNombre());
				int result = nativeQuery.executeUpdate();
				i++;
				progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),
						"actualizando Linea :" + formatProgress(i, total) + ":" + result + " registros afectados.");
			}

			em.flush();
			em.getTransaction().commit();

			logger.debug("->commit the collection for Linea OK!!");
		} catch (Exception ex) {
			//logger.error( "inicializarLinea:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void inicializarIndustrias(List<Industria> industriaReceivedList, ProgressProcessListener progressListener) throws Exception {
		logger.debug("->inicializarIndustrias: V5");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for Industria");
			em.getTransaction().begin();
			int total = industriaReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;
			for (Industria industria : industriaReceivedList) {
				Query nativeQuery = em.createNativeQuery("INSERT INTO INDUSTRIA(ID,NOMBRE) VALUES (?,?)");
				nativeQuery.setParameter(1, industria.getId());
				nativeQuery.setParameter(2, industria.getNombre());
				int result = nativeQuery.executeUpdate();
				i++;

				progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),
						"actualizando Industria :" + formatProgress(i, total) + ":" + result + " registros afectados.");
			}
			em.flush();
			em.getTransaction().commit();

			logger.debug("->commit the collection for Industria OK!!");
		} catch (Exception ex) {
			//logger.error( "inicializarIndustrias:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void inicializarMarcas(List<Marca> marcaReceivedList, ProgressProcessListener progressListener) throws Exception {
		logger.debug("->inicializarMarcas: ");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for Marca");
			em.getTransaction().begin();
			int total = marcaReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;
			for (Marca marca : marcaReceivedList) {
				Marca marcaNueva = new Marca();
				Industria marcaIndustriaNueva = em.find(Industria.class, marca.getIndustria().getId());
				if (marcaIndustriaNueva == null) {

					List<Linea> lineaList = em.createQuery("select l from Linea l").getResultList();

					logger.warn("there is not fucking working : listing Linea");
					for (Linea l : lineaList) {
						logger.warn("->\tlinea:" + l.getId() + ":" + l.getNombre());
					}

					List<Industria> industriaList = em.createQuery("select e from Industria e").getResultList();

					logger.warn("there is not fucking working : listing Industria");
					for (Industria e : industriaList) {
						logger.warn("->industria:" + e.getId() + ":" + e.getNombre());
					}

					throw new RollbackException("->for Marca can't be fucking null: linea and/or industria");
				} else {
					Query nativeQuery = em.createNativeQuery("INSERT INTO MARCA(ID,INDUSTRIA_ID,NOMBRE) VALUES (?,?,?)");
					nativeQuery.setParameter(1, marca.getId());
					nativeQuery.setParameter(2, marca.getIndustria().getId());
					nativeQuery.setParameter(3, marca.getNombre());

					int result = nativeQuery.executeUpdate();
					i++;
					progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),
							"actualizando Marcas :" + formatProgress(i, total));
				}
			}
			em.flush();
			em.getTransaction().commit();

			logger.debug("->commit the collection for Marca OK!!");
		} catch (Exception ex) {
			//logger.error( "inicializarMarcas:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void inicializarProductos(List<Producto> productoReceivedList, ProgressProcessListener progressListener) throws Exception {
		logger.debug("->inicializarProductos: ");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for Producto");
			em.getTransaction().begin();
			int total = productoReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;
			for (Producto producto : productoReceivedList) {
				//logger.debug("-->> producto:"+producto);
				Producto productoNuevo = new Producto();
				Marca productoMarcaNueva = em.find(Marca.class, producto.getMarca().getId());
				Linea productoLineaNueva = null;

				if (productoMarcaNueva == null) {
					List<Marca> marcaList = em.createQuery("select m from Marca m").getResultList();

					logger.warn("There is not fucking working : listing Marcas, searching Marca.id="+producto.getMarca().getId());
					for (Marca m : marcaList) {
						logger.warn("->marca:" + m.getId() + ":" + m.getNombre());
					}

					throw new RollbackException("->for Producto can't be fucking null: productoMarcaNueva[" + producto.getMarca().getId() + "]=" + productoMarcaNueva);
				} else if (productoLineaNueva == null) {
					List<Linea> lineaList = em.createQuery("select m from Linea m").getResultList();

					logger.warn("There is not fucking working : listing Marcas, searching Marca.id="+producto.getMarca().getId());
					for (Linea l : lineaList) {
						logger.warn("->linea:" + l.getId() + ":" + l.getNombre());
					}

					throw new RollbackException("->for Producto can't be fucking null: productoLineaNueva=" + productoLineaNueva);
				} else {
					Query nativeQuery = em.createNativeQuery("INSERT INTO PRODUCTO(ID,CODIGO_BARRAS,MARCA_ID,LINEA_ID,NOMBRE,PRESENTACION,UNIDADES_POR_CAJA,UNIDAD_MEDIDA,COSTO,COSTO_VENTA,PRECIO_BASE,CONTENIDO,ABREBIATURA) "
							+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
					int npc=1;
					nativeQuery.setParameter(npc++, producto.getId());
					nativeQuery.setParameter(npc++, producto.getCodigoBarras());
					nativeQuery.setParameter(npc++, producto.getMarca().getId());
					
					nativeQuery.setParameter(npc++, producto.getNombre());
					nativeQuery.setParameter(npc++, producto.getPresentacion());					
					nativeQuery.setParameter(npc++, producto.getUnidadesPorCaja());
					nativeQuery.setParameter(npc++, producto.getUnidadMedida());
					nativeQuery.setParameter(npc++, producto.getCosto());
					nativeQuery.setParameter(npc++, producto.getCostoVenta());
					nativeQuery.setParameter(npc++, 0);
					nativeQuery.setParameter(npc++, producto.getContenido());
					nativeQuery.setParameter(npc++, producto.getAbrebiatura());
					
					int result = nativeQuery.executeUpdate();

					i++;
					progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),
							"actualizando Productos :" + formatProgress(i, total));
				}
			}

			em.flush();
			em.getTransaction().commit();

			logger.debug("->commit the collection for Producto OK!!");
		} catch (Exception ex) {
			//logger.error( "inicializarProductos:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}
	
	public void inicializarAlmacenProductos(List<AlmacenProducto> almacenProductoReceivedList, ProgressProcessListener progressListener) throws Exception {
		logger.debug("->inicializarAlmacenProductos: ");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for Producto");
			em.getTransaction().begin();
			int total = almacenProductoReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;
			for (AlmacenProducto ap : almacenProductoReceivedList) {
				//logger.debug("-->> ap:"+ap);
				Query nativeQuery = em.createNativeQuery("INSERT INTO ALMACEN_PRODUCTO(ID,ALMACEN_ID,PRODUCTO_ID,CANTIDAD_ACTUAL,PRECIO_VENTA,PRECIO_MAYOREO) VALUES(?,?,?,?,?,?)");
				int npc=1;
				nativeQuery.setParameter(npc++, ap.getId());
				nativeQuery.setParameter(npc++, ap.getAlmacen().getId());
				nativeQuery.setParameter(npc++, ap.getProducto().getId());				
				nativeQuery.setParameter(npc++, ap.getCantidadActual());				
				nativeQuery.setParameter(npc++, ap.getPrecioVenta());
				nativeQuery.setParameter(npc++, ap.getPrecioMayoreo());
				
				int result = nativeQuery.executeUpdate();

				i++;
				progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),
						"actualizando AlmacenProducto :" + formatProgress(i, total));				
			}

			em.flush();
			em.getTransaction().commit();

			logger.debug("->commit the collection for Producto OK!!");
		} catch (Exception ex) {
			//logger.error( "inicializarProductos:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void inicializarMultimedio(List<Multimedio> multimedioReceivedList, ProgressProcessListener progressListener) throws Exception {
		logger.debug("->inicializarMultimedio: ");

		if (multimedioReceivedList == null) {
			logger.warn("->multimedioReceivedList is null !");
			return;
		}

		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for Multimedio");
			em.getTransaction().begin();
			int total = multimedioReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;
			byte[] contenido = null;
			ByteArrayInputStream baos = null;
			for (Multimedio multimedio : multimedioReceivedList) {
				/*
				contenido = multimedio.getContenido();
				if (contenido != null) {
					logger.debug("->\tcontenido Multimedio(" + multimedio.getId() + "): " + contenido.length + " bytes");
				}
				*/
				//baos = new ByteArrayInputStream(contenido);
				Query nativeQuery = em.createNativeQuery("INSERT INTO MULTIMEDIO(ID,MIME_TYPE,NOMBRE_ARCHIVO) VALUES(?,?,?,?)");
				nativeQuery.setParameter(1, multimedio.getId());
				//nativeQuery.setParameter(2, multimedio.getContenido());
				nativeQuery.setParameter(3, multimedio.getMimeType());
				nativeQuery.setParameter(4, multimedio.getNombreArchivo());
				int result = nativeQuery.executeUpdate();
				em.flush();
				i++;
				Collection<Producto> productoCollection = multimedio.getProductoCollection();

				if (productoCollection != null) {
					for (Producto producto : productoCollection) {
						Query nativeQueryInner = em.createNativeQuery("INSERT INTO PRODUCTO_MULTIMEDIO(PRODUCTO_ID,MULTIMEDIO_ID) VALUES(?,?)");
						nativeQueryInner.setParameter(1, producto.getId());
						nativeQueryInner.setParameter(2, multimedio.getId());
						nativeQueryInner.executeUpdate();
					}
				}

				progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),
						"actualizando Multimedio :" + formatProgress(i, total) + ":" + result + " registros afectados.");
			}

			em.getTransaction().commit();

			logger.debug("->commit the collection for Multimedio OK!!");

		} catch (Exception ex) {
			//logger.error( "inicializarMultimedio:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void inicializarFormaDePago(List<FormaDePago> formaDePagoReceivedList, ProgressProcessListener progressListener) throws Exception {

		logger.debug("->inicializarFormaDePagos: ");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for FormaDePago");
			em.getTransaction().begin();
			int total = formaDePagoReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;
			for (FormaDePago formaDePago : formaDePagoReceivedList) {
				Query nativeQuery = em.createNativeQuery("INSERT INTO FORMA_DE_PAGO(ID,DESCRIPCION) VALUES (?,?)");
				nativeQuery.setParameter(1, formaDePago.getId());
				nativeQuery.setParameter(2, formaDePago.getDescripcion());
				int result = nativeQuery.executeUpdate();
				i++;
				progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),
						"actualizando FormaDePago :" + formatProgress(i, total) + ":" + result + " registros afectados.");
			}
			em.flush();
			em.getTransaction().commit();

			logger.debug("->commit the collection for FormaDePago OK!!");
		} catch (Exception ex) {
			//logger.error( "inicializarFormaDePago:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void inicializarSucursal(List<Sucursal> sucursalReceivedList, ProgressProcessListener progressListener) throws Exception {

		logger.debug("->inicializarSucursal: ");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for Sucursal");
			em.getTransaction().begin();
			int total = sucursalReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;
			for (Sucursal sucursal : sucursalReceivedList) {
				logger.debug("\t-->>Sucursal: "+sucursal.getId());
				Query nativeQuery = em.createNativeQuery("INSERT INTO SUCURSAL (ID,ID_PADRE,NOMBRE,CALLE,NUM_INTERIOR,NUM_EXTERIOR,POBLACION_ID,TELEFONOS,COMENTARIOS) VALUES("+
						"?,"+(sucursal.getSucursal()!=null?"?,":"NULL,")+"?,?,?,?,?,?,?)");
				
				int paramCounter = 0;
				
				nativeQuery.setParameter(++paramCounter, sucursal.getId());
				if(sucursal.getSucursal()!=null){
					nativeQuery.setParameter(++paramCounter, sucursal.getSucursal().getId());
				}
				nativeQuery.setParameter(++paramCounter, sucursal.getNombre());
				nativeQuery.setParameter(++paramCounter, sucursal.getCalle());
				nativeQuery.setParameter(++paramCounter, sucursal.getNumExterior());
				nativeQuery.setParameter(++paramCounter, sucursal.getNumInterior());
				nativeQuery.setParameter(++paramCounter, sucursal.getPoblacion().getId());
				nativeQuery.setParameter(++paramCounter, sucursal.getTelefonos());
				nativeQuery.setParameter(++paramCounter, sucursal.getComentarios());
				
				int result = nativeQuery.executeUpdate();
				i++;
				
				
				Collection<Almacen> almacenCollection = sucursal.getAlmacenCollection();
				
				for(Almacen a: almacenCollection){
					logger.debug("\t\t-->>Almacen: "+a.getId());
					Query nativeQuery2 = em.createNativeQuery("INSERT INTO ALMACEN(ID,SUCURSAL_ID,TIPO_ALMACEN) VALUES(?,?,?)");
					nativeQuery2.setParameter(1, a.getId());
					nativeQuery2.setParameter(2, sucursal.getId());
					nativeQuery2.setParameter(3, a.getTipoAlmacen());
					
					nativeQuery2.executeUpdate();
				}
				Collection<Usuario> usuarioCollection = sucursal.getUsuarioCollection();
				
				for(Usuario u: usuarioCollection){
					logger.debug("\t\t-->>Usuario: "+u.getUsuarioId());
					Query nativeQuery3 = em.createNativeQuery("UPDATE USUARIO SET SUCURSAL_ID=? WHERE USUARIO_ID=?");
					nativeQuery3.setParameter(1, sucursal.getId());
					nativeQuery3.setParameter(2, u.getUsuarioId());
					
					nativeQuery3.executeUpdate();
				}
				
				progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),
						"actualizando Sucursal :" + formatProgress(i, total) + ":" + result + " registros afectados.");
				
				em.flush();
				logger.debug("\t<<---------- OK sucursal ");
			}
			
			em.getTransaction().commit();

			logger.debug("->commit the collection for Sucursal OK!!");
		} catch (Exception ex) {
			//logger.error( "inicializarSucursal:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void inicializarEstado(List<Estado> estadoReceivedList, ProgressProcessListener progressListener) throws Exception {
		logger.debug("->inicializarEstado: ");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for Estado");
			em.getTransaction().begin();
			int total = estadoReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;
			for (Estado estado : estadoReceivedList) {
				Query nativeQuery = em.createNativeQuery("INSERT INTO ESTADO(ID,DESCRIPCION) VALUES (?,?)");
				nativeQuery.setParameter(1, estado.getId());
				nativeQuery.setParameter(2, estado.getDescripcion());
				int result = nativeQuery.executeUpdate();
				i++;
				progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),
						"actualizando Estado :" + formatProgress(i, total) + ":" + result + " registros afectados.");
			}
			em.flush();
			em.getTransaction().commit();

			logger.debug("->commit the collection for Estado OK!!");
		} catch (Exception ex) {
			//logger.error( "inicializarEstado:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}

	public void inicializarCliente(List<Cliente> clienteReceivedList, ProgressProcessListener progressListener) throws Exception {
		logger.debug("->inicializarClientes: V5");
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			logger.debug("->Ok prepare to insert the collection for Cliente");
			em.getTransaction().begin();

			List<Cliente> clientes = em.createQuery("select c from Cliente c").getResultList();
			logger.debug("->Clientes currently found:" + clientes.size());
			for (Cliente c : clientes) {
				logger.debug("->Cliente currently found:" + c);
			}

			int total = clienteReceivedList.size();
			int percIni = progressListener.getProgress();
			int i = 0;

			BeanUtils bu = new BeanUtils();
			int maxId = -1;
			for (Cliente cliente : clienteReceivedList) {

				logger.debug("->Cliente toInsert:" + bu.describe(cliente));
				String preparedStatementQuery = "INSERT INTO CLIENTE("
						+ "ID,RFC,FECHA_CREACION,RAZON_SOCIAL,"
						+ "NOMBRE_ESTABLECIMIENTO,CALLE,NUM_INTERIOR,NUM_EXTERIOR,POBLACION_ID,"
						+ "TELEFONOS,FAXES,TELEFONOS_MOVILES,EMAIL,PLAZO_DE_PAGO,"
						+ "URL,OBSERVACIONES,DESCRIPCION_RUTA) VALUES "
						+ "(?,?,?,?,"
						+ (cliente.getNombreEstablecimiento() != null ? "?," : "NULL,")
						+ "?,"
						+ (cliente.getNumInterior() != null ? "?," : "NULL,")
						+ (cliente.getNumInterior() != null ? "?," : "NULL,")
						+ "?,"
						+ (cliente.getTelefonos() != null ? "?," : "NULL,")
						+ (cliente.getEmail() != null ? "?," : "NULL,")
						+ (cliente.getUrl() != null ? "?," : "NULL,")
						+ (cliente.getObservaciones() != null ? "?," : "NULL,");

				logger.debug("\t->query:" + preparedStatementQuery);

				Query nativeQuery = em.createNativeQuery(preparedStatementQuery);


				int paramNQ = 0;
				maxId = cliente.getId() > maxId ? cliente.getId() : maxId;

				nativeQuery.setParameter(++paramNQ, cliente.getId());
				nativeQuery.setParameter(++paramNQ, cliente.getRfc());
				nativeQuery.setParameter(++paramNQ, cliente.getFechaCreacion());
				nativeQuery.setParameter(++paramNQ, cliente.getRazonSocial());
				if (cliente.getNombreEstablecimiento() != null) {
					nativeQuery.setParameter(++paramNQ, cliente.getNombreEstablecimiento());
				}
				nativeQuery.setParameter(++paramNQ, cliente.getCalle());
				if (cliente.getNumInterior() != null) {
					nativeQuery.setParameter(++paramNQ, cliente.getNumInterior());
				}
				if (cliente.getNumExterior() != null) {
					nativeQuery.setParameter(++paramNQ, cliente.getNumExterior());
				}
				nativeQuery.setParameter(++paramNQ, cliente.getPoblacion().getId()-1);
				if (cliente.getTelefonos() != null) {
					nativeQuery.setParameter(++paramNQ, cliente.getTelefonos());
				}
				if (cliente.getEmail() != null) {
					nativeQuery.setParameter(++paramNQ, cliente.getEmail());
				}
				if (cliente.getUrl() != null) {
					nativeQuery.setParameter(++paramNQ, cliente.getUrl());
				}
				if (cliente.getObservaciones() != null) {
					nativeQuery.setParameter(++paramNQ, cliente.getObservaciones());
				}
				int result = nativeQuery.executeUpdate();
				i++;
				progressListener.updateProgress((int) Math.ceil(percIni + ((double) i / (double) total) * 5),
						"actualizando Cliente :" + formatProgress(i, total) + ":" + result + " registros afectados.");
			}
			em.flush();

			logger.debug("============>>> Cleinte: maxId=" + maxId);

			Query nativeQueryUpdateHUK = em.createNativeQuery("update HIBERNATE_UNIQUE_KEY set NEXT_HI=?");
			nativeQueryUpdateHUK.setParameter(1, maxId + 1);

			int resultUpdateHUK = nativeQueryUpdateHUK.executeUpdate();

			logger.debug("============>>> HIBERNATE_UNIQUE_KEY rows affected=" + resultUpdateHUK);

			em.flush();

			em.getTransaction().commit();

			logger.debug("->commit the collection for Cliente OK!!");
		} catch (Exception ex) {
			//logger.error( "inicializarCliente:", ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
				logger.debug("->Rollback executed!!");
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
				logger.debug("->Ok, Entity Manager Closed");
			}
		}
	}
	static DecimalFormat df = new DecimalFormat("### %");

	static String formatProgress(int d, int t) {
		return df.format((double) d / (double) t);
	}
	//==========================================================================

	public static void copySimpleProperties(Object a, Object b) {
		copySimpleProperties(a, b, new ArrayList<String>());
	}

	public static void copySimpleProperties(Object a, Object b, String[] propsNot2Copy) {
		copySimpleProperties(a, b, Arrays.asList(propsNot2Copy));
	}

	public static void copySimpleProperties(Object a, Object b, List<String> propsNot2Copy) {
		try {
			//logger.debug("->copySimpleProperties(Object a class" + a.getClass() + ",Object b class" + b.getClass() + ")");

			Class ca = a.getClass();
			Class cb = b.getClass();

			if (ca.isInstance(cb)) {
				//throw new IllegalArgumentException("Diferent Classes:" + ca.getClass() + " !=" + cb.getClass());
			}

			Field[] fcb = cb.getDeclaredFields();
			for (Field f : fcb) {
				if (propsNot2Copy.contains(f.getName())) {
					//logger.debug("\t->exclude:" + f);
					continue;
				}

				int mod = f.getModifiers();
				if ((mod & Modifier.PRIVATE) != 0
						&& (mod & Modifier.FINAL) == 0
						&& (mod & Modifier.STATIC) == 0
						&& !f.getName().toLowerCase().endsWith("collection")
						&& !f.getName().toLowerCase().endsWith("list")) {

					String setterName = "set"
							+ f.getName().substring(0, 1).toUpperCase()
							+ f.getName().substring(1);

					Method mSetter = ca.getMethod(setterName, new Class[]{f.getType()});

					String getterName = "get"
							+ f.getName().substring(0, 1).toUpperCase()
							+ f.getName().substring(1);

					Method mGetter = cb.getMethod(getterName, new Class[]{});

					Object r = mGetter.invoke(b);
					mSetter.invoke(a, r);
					Object rx = mGetter.invoke(a);
					//logger.debug("x." + setterName + "(bean." + getterName + "());");
					//logger.debug("\t=> invoke a."+mSetter+"( b."+mGetter+") = "+r+" => result :"+rx);
				}
			}
		} catch (Exception e) {
			//logger.error("Some has wrong", e);
			e.printStackTrace(System.err);
		}
	}
}
