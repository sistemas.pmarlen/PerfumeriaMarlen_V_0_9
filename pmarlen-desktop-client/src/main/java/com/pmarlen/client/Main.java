/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client;

import com.pmarlen.businesslogic.exception.UpdateBugFixingException;
import com.pmarlen.businesslogic.exception.UpdateInminentException;
import com.pmarlen.client.controller.LoginControl;
import com.pmarlen.client.controller.PreferencesController;
import com.pmarlen.client.view.SplashWindow;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.Perfil;
import com.pmarlen.model.beans.Usuario;
import com.pmarlen.model.controller.UsuarioJPAController;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractXmlApplicationContext;

/**
 *
 * @author praxis
 */
public class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);   
    private static SplashWindow splashWindow;
    
    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        Date currDate      = new Date();
		String masterHost  = null;
		String printerType = null;
		String btAdress    = null;
		String numCaja     = null;
		String sucursalId = null;
		
		boolean runningAsSlave   = false;
		boolean justPrintVersion =false;
		logger.info("==> 1:start:" + sdf.format(currDate));

        ApplicationStarter applicationStarter = null;

        try {
            splashWindow = new SplashWindow(ApplicationInfo.getInstance().getVersion());
            boolean hideSplash = false;
			int countArgs=0;
			for(String arg: args){
				logger.debug("=>> 1.1: args["+(countArgs++)+"]="+arg);
				if( arg.equalsIgnoreCase("-noSplash")){
					hideSplash = true;
				} else if( arg.startsWith("-masterHost=")){
					masterHost = arg.split("=")[1];
				} else if( arg.startsWith("-runningAsSlave=true")){
					runningAsSlave = true;
				} else if( arg.startsWith("-printerType=")){
					printerType = arg.split("=")[1];
				} else if( arg.startsWith("-btAdress=")){
					btAdress = arg.split("=")[1];
				} else if( arg.startsWith("-numCaja=")){
					numCaja = arg.split("=")[1];
				} else if( arg.startsWith("-sucursalId=")){
					sucursalId = arg.split("=")[1];
				} else if( arg.startsWith("-version")){
					justPrintVersion = true;
				}
			}
			
			if(args.length == 1 && justPrintVersion) {
				System.out.println(Constants.getServerVersion());
				System.exit(0);
			}
			
            if(!hideSplash) {       
		        splashWindow.centerInScreenAndSetVisible();
            } else {
				logger.info("\t=>>we don't display Splash");
			}
			
			
			applicationStarter = ApplicationStarter.getInstance();

			
            applicationStarter.beginProcess(splashWindow,masterHost,runningAsSlave);
        } catch (BusinessException e) {
			e.printStackTrace(System.err);
            logger.error("Error: =" + e.getMessage(), ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_TITLE"), e);
			JOptionPane.showMessageDialog(splashWindow, e.getMessage(), e.getTitle(), JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        } catch (UpdateBugFixingException e) {
			//e.printStackTrace(System.err);
			//logger.error("UpdateBugFixing:" + e.getMessage(), ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_TITLE"), e);
			JOptionPane.showMessageDialog(splashWindow, "Acción a relizar:"+e.getMessage(),"Checar Versión",  JOptionPane.WARNING_MESSAGE);
			applicationStarter.updateBugFixing();
			JOptionPane.showMessageDialog(splashWindow, "Actualización descargada e instalada, \nPor favor, reinicie la aplicación.", "Actualización", JOptionPane.INFORMATION_MESSAGE);
			System.exit(2);
		} catch (UpdateInminentException e) {
            //e.printStackTrace(System.err);
			//logger.error("UpdateInminent:" + e.getMessage(), ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_TITLE"), e);
			JOptionPane.showMessageDialog(splashWindow, "Actualizacion inminente:"+e.getMessage(), "Checar Versión", JOptionPane.WARNING_MESSAGE);            
			System.exit(3);
        } catch (Exception e) {			
			e.printStackTrace(System.err);
            logger.error("Error: lm2=" + e, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_TITLE"), e);
			JOptionPane.showMessageDialog(splashWindow, e.getMessage(), "Incio", JOptionPane.ERROR_MESSAGE);
            System.exit(3);	
        }
        logger.info("==>> 2: Ready to login.");

        AbstractXmlApplicationContext context = applicationStarter.getContext();
		
        logger.info("==>> 3: context is null ?"+(context == null));
        UsuarioJPAController usuarioJPAController = context.getBean("usuarioJPAController", UsuarioJPAController.class);        
        logger.info("==>> 4: usuarioJPAController=" + usuarioJPAController);
        ApplicationLogic applicationLogic = context.getBean("applicationLogic", ApplicationLogic.class);        
        logger.info("==>> 5: applicationLogic=" + applicationLogic);
		
		boolean preferencesChangeAtStart = false;
		
		if(printerType!=null && printerType.equalsIgnoreCase("T")){
			logger.info("==>> 5.1: cambiarTipoImpresionTermicaPOS");
			applicationLogic.cambiarTipoImpresionTermicaPOS();
			preferencesChangeAtStart = true;
		} else if(printerType!=null && printerType.equalsIgnoreCase("B")){
			logger.info("==>> 5.2: cambiarTipoImpresionBlueTooth");
			applicationLogic.cambiarTipoImpresionBlueTooth();
			preferencesChangeAtStart = true;
			if(btAdress != null){
				logger.info("==>> 5.2.1: setBTAdress: btAdress <="+btAdress);
				applicationLogic.getPreferences().
						setProperty(PreferencesController.PRINTERBLUETOOTHADDRESS, btAdress);
				
			}
		} else if(numCaja != null) {
			preferencesChangeAtStart = true;
			logger.info("==>> 5.3: setNumCaja: numCaja <="+numCaja);
				applicationLogic.getPreferences().
						setProperty(PreferencesController.NUMCAJA, numCaja);
		} else if(sucursalId != null) {
			preferencesChangeAtStart = true;
			logger.info("==>> 5.4: setSucursalId: sucursalId <="+sucursalId);
				applicationLogic.getPreferences().
						setProperty(PreferencesController.SUCURSAL, sucursalId);
		}
		
		if(preferencesChangeAtStart) {
			applicationLogic.updatePreferences();
		}
        
        List<Usuario> findUsuarioEntities = usuarioJPAController.findUsuarioEntities();

        for (Usuario usuario : findUsuarioEntities) {
            try {
                //logger.info("\t==>> 5: findUsuarioEntities[]:" + BeanUtils.describe(usuario));
                //logger.info("\t==>> 5: findUsuarioEntities[]:" + usuario+", perfiles="+usuario.getPerfilCollection()+", pedidoVentaCollection="+usuario.getPedidoVentaCollection());
                logger.info("\t=>> 1. For Usuario:"+usuario);
                Collection<Perfil> perfilCollection = usuario.getPerfilCollection();
                for(Perfil perfil: perfilCollection){
                    logger.info("\t\t=>> 1. fecthing Perfil:"+perfil);
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        splashWindow.setVisible(false);
        splashWindow.dispose();
        
        LoginControl loginControl = context.getBean("loginControl", LoginControl.class);        

        loginControl.estadoInicial();
    }
}
