/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client;

import com.pmarlen.client.view.SplashWindow;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.derby.drda.NetworkServerControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alfredo
 */
public class DBStarter {

	private static Logger logger = LoggerFactory.getLogger(DBStarter.class);

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
		Date currDate = new Date();
		String masterHost = null;
		String masterHostLocaly = null;
		logger.info("==> 1:start:" + sdf.format(currDate));

		try {
			boolean hideSplash = false;
			int countArgs = 0;
			for (String arg : args) {
				logger.debug("=>> 1.1: args[" + (countArgs++) + "]=" + arg);
				if (arg.startsWith("-masterHostLocaly=")) {
					masterHostLocaly = arg.split("=")[1];
				}
			}

			if (masterHostLocaly != null) {
				final String masterHostIsHere = masterHostLocaly;
				logger.info("\t=>>Derby database Server, ..start");
				NetworkServerControl.main(new String[]{"start", "-h", masterHostIsHere});
				System.exit(0);
			}
		} catch (Exception ex) {
			logger.error("", ex);
		}
	}
}