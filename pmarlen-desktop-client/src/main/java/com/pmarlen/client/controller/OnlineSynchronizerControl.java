/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.controller;

import com.pmarlen.businesslogic.exception.UpdateBugFixingException;
import com.pmarlen.businesslogic.exception.UpdateInminentException;
import com.pmarlen.client.ApplicationInfo;
import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.BusinessException;
import com.pmarlen.client.FirstDataSynchronizer;
import com.pmarlen.client.WebServiceConnectionConfig;
import com.pmarlen.model.Constants;
import com.pmarlen.security.OnlineSessionInfo;
import com.pmarlen.wscommons.services.DesktopSession;
import com.pmarlen.wscommons.services.GetListDataBusiness;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author alfred
 */
@Controller("onlineSynchronizerControl")
public class OnlineSynchronizerControl extends Thread{

	private Logger logger;
	private boolean keepAlive = true;
		
	@Autowired
	private ApplicationLogic applicationLogic;

	private DesktopSession desktopSession;
	
	public OnlineSynchronizerControl() {
		logger = LoggerFactory.getLogger(OnlineSynchronizerControl.class);
		System.err.println("===============>>> try to construct OnlineSynchronizerControl ");
	}

	@Override
	public void run() {
		try {
			System.err.println("... Waitting for 1st IAmlive()");
			Thread.sleep(60000);
		} catch (InterruptedException ex) {
			logger.error("at, running, first waitting:", ex);			
		}
		
		while(keepAlive) {
			IAmlive();
			try {
				Thread.sleep(30000);
			} catch (InterruptedException ex) {
				logger.error("at, running:", ex);
				keepAlive =  false;
			}
		}
	}
	

	public void IAmlive() {
		System.err.println("===============>>> IAmlive()");
		try {
			if(desktopSession == null){
				desktopSession = WebServiceConnectionConfig.getInstance().getDesktopSession();
			}

			OnlineSessionInfo onlineSessionInfo = new OnlineSessionInfo();
			System.err.println("===============>>> Need to be rigth: "+applicationLogic.getPreferences());
		
			String numCaja  = "1"; //applicationLogic.getPreferences().getProperty("caja.num");
			String sucursal = "2"; //applicationLogic.getPreferences().getProperty("sucursal.id");

			onlineSessionInfo.setLastTimeAlive(System.currentTimeMillis());		
			onlineSessionInfo.setNumCaja(Integer.parseInt(numCaja));
			onlineSessionInfo.setSucursalId(Integer.parseInt(sucursal));
			onlineSessionInfo.setUsuarioId(applicationLogic.getApplicationSession().getUsuario().getUsuarioId());

			desktopSession.iAmAlive(onlineSessionInfo);
		} catch(Exception ex) {
			logger.error("In IAmlive:", ex);
		}
	}

	/**
	 * @param applicationLogic the applicationLogic to set
	 */
	public void setApplicationLogic(ApplicationLogic applicationLogic) {
		this.applicationLogic = applicationLogic;
	}
}
