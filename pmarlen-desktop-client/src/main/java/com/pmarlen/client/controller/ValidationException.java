/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.controller;

import javax.swing.JComponent;

/**
 *
 * @author alfredo
 */
public class ValidationException extends Exception{
	JComponent wrongComponent;
	
	ValidationException(String message, JComponent wrongComponent){
		super(message);
		this.wrongComponent=wrongComponent;
	}
	
	public JComponent getWrongComponent(){
		return this.wrongComponent;
	}
	
}
