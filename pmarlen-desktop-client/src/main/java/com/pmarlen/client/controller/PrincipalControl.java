/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.controller;

import com.pmarlen.client.ApplicationInfo;
import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.client.BusinessException;
import com.pmarlen.client.ProgressProcessListener;
import com.pmarlen.client.model.ClienteComboBoxModel;
import com.pmarlen.client.model.ClienteItemList;
import com.pmarlen.client.model.ClientesTableModel;
import com.pmarlen.client.model.FechaComboBoxModel;
import com.pmarlen.client.model.IndustriaTreeNode;
import com.pmarlen.client.model.FormaDePagoComboBoxModel;
import com.pmarlen.client.model.FormaDePagoItemList;
import com.pmarlen.client.model.LineaTreeNode;
import com.pmarlen.client.model.MarcaTreeModelBuilder;
import com.pmarlen.client.model.MarcaTreeNode;
import com.pmarlen.client.model.PedidoVentaDetalleTableModel;
import com.pmarlen.client.model.PrincipalModel;
import com.pmarlen.client.model.ProductoFastDisplayModel;
import com.pmarlen.client.model.VisorDeProductosDefaultModel;
import com.pmarlen.client.ticketprinter.GeneradorDeReportes;
import com.pmarlen.client.ticketprinter.GeneradorTarjetasPrecios;
import com.pmarlen.client.view.PrincipalForm;
import com.pmarlen.client.view.VisorDeProductosDefaultPanel;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.Almacen;
import com.pmarlen.model.beans.AlmacenProducto;
import com.pmarlen.model.beans.Cliente;
import com.pmarlen.model.beans.FormaDePago;
import com.pmarlen.model.beans.PedidoVenta;
import com.pmarlen.model.beans.PedidoVentaDetalle;
import com.pmarlen.model.beans.Perfil;
import com.pmarlen.model.beans.Producto;
import com.pmarlen.model.controller.BasicInfoDAO;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EventObject;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author alfred
 */
@Controller("principalControl")
public class PrincipalControl {

	private PrincipalForm principalForm;
	private PrincipalModel principalModel;
	String currentTextToSearch;
	private Logger logger;
	@Autowired
	private ApplicationLogic applicationLogic;
	@Autowired
	private BasicInfoDAO basicInfoDAO;
	@Autowired
	private MarcaTreeModelBuilder marcaTreeModelBuilder;
	@Autowired
	private EdicionClientesControl edicionClientesControl;
	private static final int CAPTURA_MODO_CODIGO_BARRAS = 1;
	private static final int CAPTURA_MODO_NOMBRE = 2;
	private int capturaModo;
	private int tipoReporte;
	private boolean isAdmin = false;
	private DecimalFormat dfChecadorPrecios;	
	public PrincipalControl() {
		logger = LoggerFactory.getLogger(PrincipalControl.class);
	}

//    public static PrincipalControl getInstance() {
//        if (instance == null) {
//            instance = new PrincipalControl();
//        }
//
//        return instance;
//    }
	public void setup() {
		logger.debug("setup():");
		List<Producto> listProductosVacios = null;
		currentTextToSearch = "";
		principalForm = new PrincipalForm();
		principalModel = new PrincipalModel();
		
		final Collection<Perfil> perfilCollection = applicationLogic.getSession().getUsuario().getPerfilCollection();
		for (Perfil perfil : perfilCollection) {
			if (perfil.getId().equals("admin") || perfil.getId().equals("root")) {
				isAdmin = true;
			}
		}
		tipoReporte = 0;
		//=====================MENUS=======================
		principalForm.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exitByClick();
			}
		});
		principalForm.getExitMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exit();
			}
		});
		principalForm.getAcercaDeMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new JOptionPane().showMessageDialog(principalForm,
						"Perfumeria Marlen - Caja\nxpressosystems.com\n Developer: Alfredo Estrada", "Acerca de",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		principalForm.getViewDetailMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verDetalle();
			}
		});
		principalForm.getBuscarXCBMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeInputMethodCB();
			}
		});
		principalForm.getBuscarXNombreMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeInputMethodNombre();
			}
		});
		principalForm.getTestPrintMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				testPrinter();
			}
		});

		principalForm.getPreferncesMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PreferencesController.showPreferencesDialog(principalForm, applicationLogic);
			}
		});

		principalForm.getPorFechaHoyRadioBtn().setSelected(true);

		principalForm.getPorFechaHoyRadioBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
				Date today = new Date();
				principalForm.getFechaReporteInicio().setSelectedItem(sdf.format(today));
				principalForm.getFechaReporteFin().setSelectedItem(sdf.format(today));

				principalForm.getFechaReporteInicio().setEnabled(false);
				principalForm.getFechaReporteFin().setEnabled(false);
			}
		});
		principalForm.getPorIntervaloRadioBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				principalForm.getFechaReporteInicio().setEnabled(true);
				principalForm.getFechaReporteFin().setEnabled(true);
			}
		});

		principalForm.getVerVentasXTicket().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tipoReporte = 1;
				((TitledBorder) principalForm.getPanelParametrosReporte().getBorder()).setTitle("Ventas Por Ticket");
				verReporteVentasXTicket();
			}
		});

		principalForm.getReimprimirTicketBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reimprimirTicket();
			}
		});

		principalForm.getTablaReporte().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		principalForm.getActualizarReporteBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tipoReporte == 1) {
					principalForm.getReimprimirTicketBtn().setEnabled(true);
					refreshVentasXTicket();
				} else if (tipoReporte == 2) {
					principalForm.getReimprimirTicketBtn().setEnabled(false);
					refreshVentasXUsuario();
				} else if (tipoReporte == 3) {
					principalForm.getReimprimirTicketBtn().setEnabled(false);
					refreshVentasXProducto();
				}
			}
		});
		if (isAdmin) {
			principalForm.getGeneradorTarjetasPrecios().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (isFrontPanel("visorChecadorDePrecios")) {
						generaTarjetasDePreciosProductoActual();
					} else {
						generaTarjetasDePrecios();
					}
				}
			});

			principalForm.getVerVentasXUsuario().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					tipoReporte = 2;
					((TitledBorder) principalForm.getPanelParametrosReporte().getBorder()).setTitle("Ventas Por Usuario");
					verVentasXUsuario();
				}
			});
			principalForm.getVerVentasXProducto().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					tipoReporte = 3;
					((TitledBorder) principalForm.getPanelParametrosReporte().getBorder()).setTitle("Ventas Por Producto");
					verVentasXProducto();
				}
			});

			principalForm.getGenerarPDFBtn().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					generarReportePDFYMostrarlo();
				}
			});
			principalForm.getActualizarPrecioBtn().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					actualizarPrecio();
				}
			});
			dfChecadorPrecios = new DecimalFormat("###,###,##0.00");
		} else {
			//principalForm.getPreferncesMenu().setEnabled(false);
			//principalForm.getVerVentasXTicket().setEnabled(false);
			principalForm.getVerVentasXUsuario().setEnabled(false);
			principalForm.getVerVentasXProducto().setEnabled(false);
			principalForm.getGenerarPDFBtn().setEnabled(false);
			principalForm.getGeneradorTarjetasPrecios().setEnabled(false);
			principalForm.getPrecioEncontrado().setEditable(false);			
			dfChecadorPrecios = new DecimalFormat("###,###,##0.00");
			
			principalForm.getActualizarPrecioBtn().setVisible(false);
			principalForm.getActualizarPrecioBtn().remove(principalForm.getActualizarPrecioBtn());
			
		}

		principalForm.getViewClientesMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verClientes();
			}
		});
		principalForm.getDeleteItemPedidoMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				eliminarProductoDePedido();
			}
		});
		principalForm.getProcederPedidoMenu().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				procederPedidoSucursal();
			}
		});

		Almacen almacen = applicationLogic.getSession().getAlmacen();
		if (almacen.getTipoAlmacen() == Constants.ALMACEN_LINEA) {
			principalForm.getChangePedidoNormal().setSelected(true);
			principalForm.getLabel2().setText("VENTA NORMAL[" + almacen.getId() + "]");
		} else if (almacen.getTipoAlmacen() == Constants.ALMACEN_OPORTUNIDAD) {
			principalForm.getChangePedidoOportunidad().setSelected(true);
			principalForm.getLabel2().setText("VENTA DE OPORTUNIDAD[" + almacen.getId() + "]");
		}

		principalForm.getChangePedidoNormal().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Collection<Almacen> almacenCollection = applicationLogic.getSession().getSucursal().getAlmacenCollection();
				for (Almacen a : almacenCollection) {
					if (a.getTipoAlmacen() == Constants.ALMACEN_LINEA) {
						applicationLogic.getSession().setAlmacen(a);
						principalForm.getLabel2().setText("VENTA NORMAL[" + a.getId() + "]");
						break;
					}
				}
			}
		});

		principalForm.getChangePedidoOportunidad().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Collection<Almacen> almacenCollection = applicationLogic.getSession().getSucursal().getAlmacenCollection();
				for (Almacen a : almacenCollection) {
					if (a.getTipoAlmacen() == Constants.ALMACEN_OPORTUNIDAD) {
						applicationLogic.getSession().setAlmacen(a);
						principalForm.getLabel2().setText("VENTA DE OPORTUNIDAD[" + a.getId() + "]");
						break;
					}
				}
			}
		});


		principalForm.getNuevoClienteBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createCliente();
			}
		});
		principalForm.getNuevoClienteAComboBox().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createCliente();
			}
		});
		principalForm.getChecadorDePreciosMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checadorDePrecios();
			}
		});

		principalForm.getCodigoBarrasBuscar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String codigoDeBarras = principalForm.getCodigoBarrasBuscar().getText().trim();
				if (codigoDeBarras.length() > 1) {
					checarPrecio(codigoDeBarras);
				}
			}
		});

		principalForm.getCodigoBarrasBuscar().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!isAdmin) {
					limpiarResultadoBusqueda();
				}
			}
			
			public void focusGained(FocusEvent e) {
				if(isAdmin){
					limpiarResultadoBusqueda();
				}
			}
		});


		principalForm.getCodigoBuscar().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				//super.focusLost(e);
				Producto producto = null;
				String codigoDeBarras = principalForm.getCodigoBuscar().getText().trim();
				try {
					producto = applicationLogic.findProductoByCodigoDeBarras(codigoDeBarras);
					updateProductoSelected(
							new ProductoFastDisplayModel(producto.getId(), producto.getNombre(), producto),
							getPrincipalForm().getCantidadCBPedida());
					principalForm.getCantidadCBPedida().requestFocus();
				} catch (Exception ex) {
					updateProductoSelected(
							null,
							getPrincipalForm().getCantidadCBPedida());
					principalForm.getCodigoBuscar().setText("");
				}
			}
		});

		principalForm.getCodigoBuscar().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Producto producto = null;
				String codigoDeBarras = principalForm.getCodigoBuscar().getText().trim();
				try {
					producto = applicationLogic.findProductoByCodigoDeBarras(codigoDeBarras);
					updateProductoSelected(
							new ProductoFastDisplayModel(producto.getId(), producto.getNombre(), producto),
							getPrincipalForm().getCantidadCBPedida());
					//principalForm.getCantidadCBPedida().requestFocus();
					agregarNProductoCB();
				} catch (Exception ex) {
					updateProductoSelected(
							null,
							getPrincipalForm().getCantidadCBPedida());
					principalForm.getCodigoBuscar().setText("");
				}
			}
		});

		principalForm.getNombreBuscar().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					if (getPrincipalForm().getProductoCBEncontrados().getModel().getSize() >= 1) {
						getPrincipalForm().getProductoCBEncontrados().setSelectedIndex(getPrincipalForm().getProductoCBEncontrados().getModel().getSize() > 1 ? 1 : 0);
						getPrincipalForm().getProductoCBEncontrados().requestFocus();
					} else {
						return;
					}
				} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					updateProductoSelected(
							null,
							getPrincipalForm().getCantidadPedida());
					getPrincipalForm().getDetallePedidoTable().requestFocus();
				}
			}
		});

		principalForm.getNombreBuscar().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (getPrincipalForm().getProductoCBEncontrados().getModel().getSize() > 0) {
					principalForm.getProductoCBEncontrados().setPopupVisible(false);
					getPrincipalForm().getCantidadPedida().requestFocus();
				}
			}
		});

		principalForm.getNombreBuscar().getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				if (!principalForm.getNombreBuscar().getText().toLowerCase().trim().equals(currentTextToSearch)) {
					currentTextToSearch = principalForm.getNombreBuscar().getText().toLowerCase().trim();
					logger.debug("->insertUpdate: currentTextToSearch=" + currentTextToSearch);
					updateProductosFound();
				}
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				if (!principalForm.getNombreBuscar().getText().toLowerCase().trim().equals(currentTextToSearch)) {
					currentTextToSearch = principalForm.getNombreBuscar().getText().toLowerCase().trim();
					logger.debug("->removeUpdate: currentTextToSearch=" + currentTextToSearch);
					updateProductosFound();
				}
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				logger.debug("->changedUpdate: " + principalForm.getNombreBuscar().getText() + " != " + currentTextToSearch + " ???");
			}
		});
		/*
		principalForm.getRecibimos().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (principalForm.getRecibimos().getText().trim().length() > 0) {
					try {
						double recibimos = Double.parseDouble(principalForm.getRecibimos().getText().trim());
						double total = calculaImporteTotal();
						if( recibimos <= total){
							throw new IllegalArgumentException("El importe recibido debe ser mayor al TOTAL.");
						}
						actualizarCambioContraRecibido(recibimos);

					} catch (NumberFormatException nfe) {
						principalForm.getRecibimos().setText("");
					} catch (IllegalArgumentException iae) {
						principalForm.getRecibimos().setText("");
						JOptionPane.showMessageDialog(principalForm, iae.getMessage(), "Confirmar Venta", JOptionPane.ERROR_MESSAGE);			
					}
				}
			}
		});
		*/
		
		principalForm.getProductoCBEncontrados().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					logger.debug("->principalForm.getProductoCBEncontrados().keyPressed(): VK_ENTER");
					if (getPrincipalForm().getProductoCBEncontrados().getModel().getSize() > 0) {
						principalForm.getProductoCBEncontrados().setPopupVisible(false);
						getPrincipalForm().getCantidadPedida().requestFocus();
					}
				} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					updateProductoSelected(null, getPrincipalForm().getCantidadPedida());
					getPrincipalForm().getDetallePedidoTable().requestFocus();
				}
			}
		});


		getPrincipalForm().getProductoCBEncontrados().setModel(new DefaultComboBoxModel(
				applicationLogic.getProducto4Display(currentTextToSearch)));
		getPrincipalForm().getProductoCBEncontrados().addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object itemSelect = e.getItem();
					updateProductoSelected((ProductoFastDisplayModel) itemSelect, getPrincipalForm().getCantidadPedida());
				}
			}
		});

		getPrincipalForm().getCantidadPedida().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					updateProductoSelected(null, getPrincipalForm().getCantidadPedida());
					getPrincipalForm().getDetallePedidoTable().requestFocus();
				}
			}
		});

		getPrincipalForm().getCantidadPedida().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				agregarNProducto();
			}
		});

		getPrincipalForm().getCantidadCBPedida().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				agregarNProductoCB();
			}
		});

		principalForm.getDetallePedidoTable().addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (principalForm.getDetallePedidoTable().getRowCount() > 0) {
					if (principalForm.getDetallePedidoTable().getSelectedRowCount() == 0) {
						principalForm.getDetallePedidoTable().setRowSelectionInterval(0, 0);
					}
				}
			}
		});
		//======================================================================
		List<Cliente> listCliente = null;
		List<FormaDePago> listFormaDePago = null;
		try {
			listCliente = basicInfoDAO.getClientesList();
			listFormaDePago =
					basicInfoDAO.getFormaDePagosList();
		} catch (Exception ex) {
			listCliente = new ArrayList<Cliente>();
			listFormaDePago =
					new ArrayList<FormaDePago>();
			logger.error("", ex);
		}

		principalForm.getCliente().setModel(new ClienteComboBoxModel(listCliente));
		principalForm.getCliente().addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				logger.debug("->cliente.itemStateChanged" + e.getItem() + ": " + e.getStateChange() + " == " + ItemEvent.SELECTED + "? " + (e.getStateChange() == ItemEvent.SELECTED));
				if (e.getStateChange() == ItemEvent.SELECTED) {
					verifyAllSelections();
				}

			}
		});
		principalForm.getFormaDePago().setModel(new FormaDePagoComboBoxModel(listFormaDePago));
		principalForm.getFormaDePago().addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				logger.debug("->formaDePago.itemStateChanged: " + e.getItem() + ": " + e.getStateChange() + " == " + ItemEvent.SELECTED + "? " + (e.getStateChange() == ItemEvent.SELECTED));
				if (e.getStateChange() == ItemEvent.SELECTED) {
					verifyAllSelections();
				}

			}
		});

		principalForm.getProcederPedidoBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//procederPedido();
				procederPedidoSucursal();
			}
		});

		principalForm.getReiniciarPedidoBtn().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reiniciarPedido();
			}
		});

		principalForm.getAvoidPedidoMenu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reiniciarPedido();
			}
		});

		principalForm.getClientesTable().setModel(new ClientesTableModel(listCliente));

		principalForm.getClientesTable().getColumnModel().getColumn(0).setPreferredWidth(40);
		principalForm.getClientesTable().getColumnModel().getColumn(1).setPreferredWidth(400);
		principalForm.getClientesTable().getColumnModel().getColumn(2).setPreferredWidth(120);
		principalForm.getClientesTable().getColumnModel().getColumn(3).setPreferredWidth(280);
		principalForm.getClientesTable().getColumnModel().getColumn(4).setPreferredWidth(75);
		principalForm.getClientesTable().getColumnModel().getColumn(5).setPreferredWidth(75);
		principalForm.getClientesTable().getColumnModel().getColumn(6).setPreferredWidth(450);
		principalForm.getClientesTable().getColumnModel().getColumn(7).setPreferredWidth(220);
		principalForm.getClientesTable().getColumnModel().getColumn(8).setPreferredWidth(180);
		principalForm.getClientesTable().getColumnModel().getColumn(9).setPreferredWidth(200);
		principalForm.getClientesTable().getColumnModel().getColumn(10).setPreferredWidth(100);

		principalForm.getClientesTable().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
					editCliente();
				} else if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
					deleteCliente();
				}

			}
		});

		principalForm.getClientesTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					editCliente();
				}
			}
		});

		try {
			iconTree1 = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/imgs/arrow_right_green_16x16.png")));
			iconTree2 = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/imgs/free_16x16.png")));
			iconTree3 = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/imgs/enterprise_16x16.png")));
			iconTree4 = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/imgs/grid_16x16.png")));
			iconTree5 = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/imgs/box_16x16.png")));
		} catch (Exception ex) {
			logger.error("", ex);
		}
		changeInputMethodCB();
	}
	static Icon iconTree1;
	static Icon iconTree2;
	static Icon iconTree3;
	static Icon iconTree4;
	static Icon iconTree5;

	/**
	 * @return the applicationLogic
	 */
	public ApplicationLogic getApplicationLogic() {
		return applicationLogic;
	}

	/**
	 * @param applicationLogic the applicationLogic to set
	 */
	public void setApplicationLogic(ApplicationLogic applicationLogic) {
		this.applicationLogic = applicationLogic;
	}

	/**
	 * @return the basicInfoDAO
	 */
	public BasicInfoDAO getBasicInfoDAO() {
		return basicInfoDAO;
	}

	/**
	 * @param basicInfoDAO the basicInfoDAO to set
	 */
	public void setBasicInfoDAO(BasicInfoDAO basicInfoDAO) {
		this.basicInfoDAO = basicInfoDAO;
	}

	public void setMarcaTreeModelBuilder(MarcaTreeModelBuilder marcaTreeModelBuilder) {
		this.marcaTreeModelBuilder = marcaTreeModelBuilder;
	}

	/**
	 * @param edicionClientesControl the edicionClientesControl to set
	 */
	public void setEdicionClientesControl(EdicionClientesControl edicionClientesControl) {
		this.edicionClientesControl = edicionClientesControl;
	}

	void maximizeWindow() {
		principalForm.setExtendedState(principalForm.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	}

	private void limpiarResultadoBusqueda() {
		principalForm.getNombreDescripcionEncontrado().setText("");
		principalForm.getCodigoBarrasBuscar().setText("");
		principalForm.getPrecioEncontrado().setText("");
		principalForm.getCodigoBarrasEncontrado().setText("");
		principalForm.getExistenciaEncontrada().setText("");
	}

	class MyMarcaTreeNodeRenderer extends DefaultTreeCellRenderer {

		public MyMarcaTreeNodeRenderer() {
		}

		public Component getTreeCellRendererComponent(
				JTree tree,
				Object value,
				boolean sel,
				boolean expanded,
				boolean leaf,
				int row,
				boolean hasFocus) {

			super.getTreeCellRendererComponent(
					tree, value, sel,
					expanded, leaf, row,
					hasFocus);
			if (!leaf && isRootNode(value)) {
				setIcon(iconTree1);
				setToolTipText("-");
			} else if (isLineaNode(value)) {
				setIcon(iconTree2);
				setToolTipText("Linea");
			} else if (isIndustriaNode(value)) {
				setIcon(iconTree3);
				setToolTipText("Industria");
			} else if (isMarcaNode(value)) {
				setIcon(iconTree4);
				setToolTipText("Marca");
			} else {
				setIcon(iconTree5);
				setToolTipText("?");
			}

			return this;
		}

		protected boolean isRootNode(Object value) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			return (node.getUserObject() instanceof String);
		}

		protected boolean isMarcaNode(Object value) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			return (node.getUserObject() instanceof MarcaTreeNode);
		}

		protected boolean isLineaNode(Object value) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			return (node.getUserObject() instanceof LineaTreeNode);
		}

		protected boolean isIndustriaNode(Object value) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			return (node.getUserObject() instanceof IndustriaTreeNode);
		}
	}

	class DiscountDVPCellEditor extends AbstractCellEditor
			implements TableCellEditor {

		final JSpinner spinner = new JSpinner();

		// Initializes the spinner.
		public DiscountDVPCellEditor() {
			spinner.setModel(new SpinnerNumberModel(0, 0, 5, 1));
		}

		// Prepares the spinner component and returns it.
		public Component getTableCellEditorComponent(JTable table, Object value,
				boolean isSelected, int row, int column) {

			double maxDesc = 0.0;
			((SpinnerNumberModel) spinner.getModel()).setMaximum(new Integer((int) Math.round(maxDesc * 100.0)));
			logger.debug("->:getTableCellEditorComponent(" + row + "," + column + "): spinner.setValue(value=" + value + " class " + value.getClass() + "):  maxDesc=" + maxDesc);
			spinner.setValue(new Integer(value.toString()));
			new Thread() {
				public void run() {
					logger.debug("->:getTableCellEditorComponent() shuld requesting fucking  foucs ?" + (!spinner.isFocusOwner()));
					for (int ifo = 0; ifo < 10 && !spinner.isFocusOwner(); ifo++) {
						logger.debug("->:getTableCellEditorComponent() requesting foucs [" + (ifo + 1) + "/10]");
						spinner.requestFocus();
						try {
							Thread.sleep(250);
						} catch (InterruptedException ex) {
						}
					}
				}
			}.start();

			return spinner;
		}

		public boolean isCellEditable(EventObject evt) {
			logger.debug("-> DiscountDVPCellEditor: event " + evt.getClass() + " = " + evt);
			if (evt instanceof MouseEvent) {
				return ((MouseEvent) evt).getClickCount() >= 2;
			} else if (evt instanceof KeyEvent) {
				KeyEvent keyEvent = (KeyEvent) evt;
				boolean canEdit = keyEvent.getKeyCode() == KeyEvent.VK_F2;
				logger.debug("-> DiscountDVPCellEditor: EDIT Cell Render ?" + canEdit);
				return canEdit;
			} else if (evt instanceof ActionEvent) {
				ActionEvent actionEvent = (ActionEvent) evt;

			}
			return false;
		}

		// Returns the spinners current value.
		public Object getCellEditorValue() {
			return spinner.getValue();
		}
	}

	void refreshClientesList() {
		List<Cliente> listCliente = null;

		try {
			listCliente = basicInfoDAO.getClientesList();
		} catch (Exception ex) {
			listCliente = new ArrayList<Cliente>();
			logger.error("", ex);
		}

		principalForm.getCliente().setModel(new ClienteComboBoxModel(listCliente));
		principalForm.getClientesTable().setModel(new ClientesTableModel(listCliente));
		principalForm.getClientesTable().updateUI();
		principalForm.getCliente().updateUI();

	}

	public void updateDetallePedidoTable() {
		int sizeDetail = applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().size();
		principalForm.getReiniciarPedidoBtn().setEnabled(sizeDetail > 0);
		principalForm.getAvoidPedidoMenu().setEnabled(sizeDetail > 0);
		principalForm.getDetallePedidoTable().updateUI();
		if (sizeDetail > 0) {
			boolean esFiscal = true;
			double st = 0.0;
			double si = 0.0;
			double sd = 0.0;

			for (PedidoVentaDetalle dvp : applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection()) {
				si += dvp.getCantidad() * dvp.getPrecioVenta();
			}

			double osi = 0.0;
			double osd = 0.0;
			double otax = 0.0;
			double ostt = 0.0;

			osi = si; //* (1.0 + applicationLogic.getFactorIVA());
			osd = sd; //* (1.0 + applicationLogic.getFactorIVA());
			otax = 0.0;
			ostt = (osi - osd);


			principalForm.getSubtotal().setText(ApplicationInfo.formatToCurrency(osi));
			principalForm.getDiscount().setText(ApplicationInfo.formatToCurrency(osd));
			principalForm.getTotal().setText(ApplicationInfo.formatToCurrency(ostt));

			verifyAllSelections();

			principalForm.getChangePedidoNormal().setEnabled(false);
			principalForm.getChangePedidoOportunidad().setEnabled(false);
		} else {
			principalForm.getSubtotal().setText("");
			principalForm.getDiscount().setText("");
			principalForm.getTotal().setText("");

			principalForm.getChangePedidoNormal().setEnabled(true);
			principalForm.getChangePedidoOportunidad().setEnabled(true);
		}

	}

	double calculaImporteTotal() {
		double si = 0.0;
		int sizeDetail = applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().size();

		if (sizeDetail > 0) {
			double cambio = 0.0;
			for (PedidoVentaDetalle dvp : applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection()) {
				si += dvp.getCantidad() * dvp.getPrecioVenta();
			}
		}
		return si;
	}

	void actualizarCambioContraRecibido(double recibido) {
		int sizeDetail = applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().size();
		if (sizeDetail > 0) {
//			boolean esFiscal = true;
//			double st = 0.0;
			double si = 0.0;
//			double sd = 0.0;
			double cambio = 0.0;
			for (PedidoVentaDetalle dvp : applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection()) {
				si += dvp.getCantidad() * dvp.getPrecioVenta();
			}
			cambio = recibido - si;
//			double osi = 0.0;
//			double osd = 0.0;
//			double otax = 0.0;
//			double ostt = 0.0;

//
//			if (esFiscal) {
//				osi = si;
//				osd = sd;
//				otax = (si - sd) * applicationLogic.getFactorIVA();
//				ostt = (osi - osd) + otax;
//			}

			//principalForm.getTotal().setText(ApplicationInfo.formatToCurrency(ostt));
			principalForm.getCambio().setText(ApplicationInfo.formatToCurrency(cambio));
		} else {
			principalForm.getCambio().setText("");
		}
	}

	double calculaCambioContraRecibido(double recibido) {
		int sizeDetail = applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().size();
		double cambio = 0.0;
		if (sizeDetail > 0) {
//			boolean esFiscal = true;
//			double st = 0.0;
			double si = 0.0;
//			double sd = 0.0;

			for (PedidoVentaDetalle dvp : applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection()) {
				si += dvp.getCantidad() * dvp.getPrecioVenta();
			}
			cambio = recibido - si;

		} else {
			principalForm.getCambio().setText("");
		}
		return cambio;
	}

	void toFrontPanel(String nombre) {
		CardLayout cl = (CardLayout) (principalForm.getCardPanel().getLayout());

		if (nombre.equals(principalModel.getFrontPanel())) {
			return;
		}

		principalModel.setFrontPanel(nombre);
		cl.show(principalForm.getCardPanel(), nombre);
	}

	boolean isFrontPanel(String nombre) {
		CardLayout cl = (CardLayout) (principalForm.getCardPanel().getLayout());
		return principalModel.getFrontPanel().equals(nombre);
	}

	void updateImageForDisplayInPanel(int index, VisorDeProductosDefaultModel dpdm, VisorDeProductosDefaultPanel vpdp) {
		BufferedImage bi = null;
		Producto prod = null;
		if (index != -1) {
			prod = dpdm.getSelected(index);
			bi = dpdm.getImageForDisplay(index);
			vpdp.setImageForPanelDisplayer(index);
		}

		vpdp.getVisorDeAtributos().updateLabels(prod);
	}
	List<Producto> listProductosSinSurtido;

	void verDetalle() {
		toFrontPanel("visorDePedidoActual");
		focusForCapture();
	}

	void verReporteVentasXTicket() {
		refreshRangoDeFechas();
		refreshVentasXTicket();
		toFrontPanel("visorDeReportes");
		principalForm.getDetallePedidoTable().requestFocus();
	}

	void verClientes() {
		refreshClientesList();
		toFrontPanel("visorDeClientes");
		principalForm.getClientesTable().requestFocus();
	}

	void agregarNProducto() {
		logger.debug("->agregarNProducto");
		int cantPedida = 0;
		Producto productoAgregar = null;
		try {
			productoAgregar = applicationLogic.getSession().getProductoBuscadoActual();
			cantPedida = Integer.parseInt(getPrincipalForm().getCantidadPedida().getText().trim());
			if (cantPedida > 0 && productoAgregar != null) {
				logger.debug("->add N="+cantPedida);

				applicationLogic.addProductoNToCurrentPedidoVenta(productoAgregar, cantPedida);

				updateDetallePedidoTable();
				getPrincipalForm().getNombreBuscar().setText("");
				getPrincipalForm().getNombreBuscar().requestFocus();
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			//logger.error("->agregarNProducto", e);
			JOptionPane.showMessageDialog(principalForm, e.getMessage(), "Agregar producto", JOptionPane.ERROR_MESSAGE);
			prepareForCaptureClear();
		}
		getPrincipalForm().getCantidadPedida().setText("");

	}

	void agregarNProductoCB() {
		logger.debug("->agregarNProductoCB");
		int cantPedida = 0;
		Producto productoAgregar = null;
		try {
			productoAgregar = applicationLogic.getSession().getProductoBuscadoActual();
			cantPedida = Integer.parseInt(getPrincipalForm().getCantidadCBPedida().getText().trim());
			if (cantPedida > 0 && productoAgregar != null) {
				logger.debug("->add N="+cantPedida);

				applicationLogic.addProductoNToCurrentPedidoVenta(productoAgregar, cantPedida);

				updateDetallePedidoTable();
				getPrincipalForm().getCodigoBuscar().setText("");
				getPrincipalForm().getCodigoBuscar().requestFocus();
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(principalForm, e.getMessage(), "Agregar producto", JOptionPane.ERROR_MESSAGE);
			prepareForCaptureClear();
		}
		getPrincipalForm().getCantidadCBPedida().setText("");
	}

	void changeInputMethodNombre() {
		JPanel pnl = principalForm.getBuscarCardsPanel();
		CardLayout cl = (CardLayout) pnl.getLayout();
		cl.show(pnl, "buscarNombreCard");
		capturaModo = CAPTURA_MODO_NOMBRE;
		focusForCapture();
	}

	void changeInputMethodCB() {
		JPanel pnl = principalForm.getBuscarCardsPanel();
		CardLayout cl = (CardLayout) pnl.getLayout();
		cl.show(pnl, "buscarCBCard");
		capturaModo = CAPTURA_MODO_CODIGO_BARRAS;
		focusForCapture();
	}

	private void prepareForCaptureClear() {
		getPrincipalForm().getCodigoBuscar().setText("");
		getPrincipalForm().getNombreBuscar().setText("");
		getPrincipalForm().getCantidadPedida().setText("");
		getPrincipalForm().getCantidadCBPedida().setText("");

		focusForCapture();
	}

	private void focusForCapture() {
		if (capturaModo == CAPTURA_MODO_CODIGO_BARRAS) {
			getPrincipalForm().getCodigoBuscar().requestFocus();
		} else if (capturaModo == CAPTURA_MODO_NOMBRE) {
			getPrincipalForm().getNombreBuscar().requestFocus();
		}
	}

	void verifyAllSelections() {
		int sizeDetail = applicationLogic.getSession().
				getPedidoVenta().getPedidoVentaDetalleCollection().size();
		logger.debug("->verifyAllSelections(): sizeDetail=" + sizeDetail);
		Cliente clienteSelected = null;
		FormaDePago formaDePagoSelected = null;

		if ((ClienteItemList) principalForm.getCliente().getSelectedItem() != null) {
			clienteSelected = ((ClienteItemList) principalForm.getCliente().getSelectedItem()).getCliente();
			if (clienteSelected.getId() == null) {
				clienteSelected = null;
			}
		} else {
			clienteSelected = null;
		}

		if (((FormaDePagoItemList) principalForm.getFormaDePago().getSelectedItem()) != null) {
			formaDePagoSelected = ((FormaDePagoItemList) principalForm.getFormaDePago().getSelectedItem()).getFormaDePago();
			if (formaDePagoSelected == null) {
				formaDePagoSelected = null;
			}
		} else {
			formaDePagoSelected = null;
		}


		applicationLogic.setClienteToCurrentPedidoVenta(clienteSelected);
		applicationLogic.setFormaDePagoToCurrentPedidoVenta(formaDePagoSelected);
	}

	private void validatePedidoSelections() throws ValidationException{
		int sizeDetail = applicationLogic.getSession().
				getPedidoVenta().getPedidoVentaDetalleCollection().size();

		if (sizeDetail == 0) {
			if (capturaModo == CAPTURA_MODO_CODIGO_BARRAS) {
				throw new ValidationException("Tiene que capturar un Producto por Codigo de Barras",getPrincipalForm().getCodigoBuscar());
			} else if (capturaModo == CAPTURA_MODO_NOMBRE) {
				throw new ValidationException("Tiene que capturar un Producto por Nombre",getPrincipalForm().getNombreBuscar());
			}
		}

		if (principalForm.getCliente().getSelectedIndex() == 0) {
			throw new ValidationException("Tiene que elegir un Cliente",principalForm.getCliente());
		}

		if (principalForm.getFormaDePago().getSelectedIndex() == 0) {			
			throw new ValidationException("Tiene que elegir una Forma de Pago",principalForm.getFormaDePago());
		}

		if (principalForm.getRecibimos().getText().trim().length() == 0) {
			throw new ValidationException("Tiene que capturar el Importe Recibido",principalForm.getRecibimos());
		} else {
			double recibimos = Double.parseDouble(principalForm.getRecibimos().getText().trim());
			double total = calculaImporteTotal();
			if( recibimos < total){
				throw new ValidationException("El Importe Recibido debe ser mayor o igual al TOTAL.",principalForm.getRecibimos());
			}
		}

	}
//	void procederPedido() {
//		logger.debug("->procederPedido(): confirm ?");
//
//		JComponent missingComponent = validatePedidoSelections();
//		if (missingComponent != null) {
//			missingComponent.requestFocus();
//			return;
//		}
//		int confirm = JOptionPane.showConfirmDialog(principalForm, ApplicationInfo.getLocalizedMessage("LABEL_PROCED_CONFIRMATION"),
//				ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
//				JOptionPane.YES_NO_OPTION,
//				JOptionPane.QUESTION_MESSAGE);
//		if (confirm == JOptionPane.NO_OPTION) {
//			return;
//		}
//
//		try {
//			applicationLogic.persistCurrentPedidoVenta();
//			PedidoVenta pedidoVenta = applicationLogic.getSession().getPedidoVenta();
//			String pseudoNumeroPedido = ApplicationInfo.getNumPseudoPedido(
//					applicationLogic.getSession().getPedidoVenta());
//
//			JOptionPane.showMessageDialog(principalForm,
//					ApplicationInfo.getLocalizedMessage("LABEL_PEDIDO_SAVED") + pseudoNumeroPedido,
//					ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
//					JOptionPane.INFORMATION_MESSAGE);
//
//
//			Icon printIcon = null;
//			String imgImpresora = null;
//			//imgImpresora = "/imgs/Zebra_MZ_320.jpg";
//			imgImpresora = "/imgs/Epson_tm_t20.jpg";
//			try {
//
//				printIcon = new ImageIcon(ImageIO.read(getClass().getResourceAsStream(imgImpresora)));
//			} catch (IOException ex) {
//				logger.error("", ex);
//			}
//
//			int confirmPrint = JOptionPane.showConfirmDialog(principalForm,
//					ApplicationInfo.getLocalizedMessage("LABEL_PRINT_CONFIRMATION"),
//					ApplicationInfo.getLocalizedMessage("LABEL_PRINT_TICKET"),
//					JOptionPane.YES_NO_OPTION,
//					JOptionPane.QUESTION_MESSAGE,
//					printIcon);
//
//			try {
//				applicationLogic.printTicketPedido(pedidoVenta, confirmPrint == JOptionPane.YES_OPTION,
//						principalForm.getRecibimos().getText(), principalForm.getCambio().getText());
//			} catch (BusinessException ex) {
//				JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
//						ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
//						JOptionPane.ERROR_MESSAGE);
//			}
//			logger.debug("->procederPedido(): ok estado inicial");
//			estadoInicial(true);
//		} catch (BusinessException ex) {
//			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
//					ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
//					JOptionPane.ERROR_MESSAGE);
//		}
//
//	}
//	
	long st1, st2, st3;

	void procederPedidoSucursal() {
		logger.debug("->procederPedidoSucursal(): confirm ?");
		
		try{
			validatePedidoSelections();			
		} catch(ValidationException ve ){
			logger.debug("->procederPedidoSucursal(): ValidationException:"+ve);
		
			JComponent missingComponent = ve.getWrongComponent();
			missingComponent.requestFocus();
			JOptionPane.showMessageDialog(principalForm, ve.getMessage(),
					ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		st1 = System.currentTimeMillis();
		logger.debug("->procederPedidoSucursal(): >>> SAVE > PRINT");
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {
			logger.debug("\t->procederPedidoSucursal(): persistCurrentPedidoVenta");
			PedidoVenta pedidoVenta = applicationLogic.getSession().getPedidoVenta();

			pedidoVenta.setUsuario(applicationLogic.getSession().getUsuario());
			//pedidoVenta.setCliente(applicationLogic.getSession().getCliente());

			pedidoVenta.setAlmacen(applicationLogic.getSession().getAlmacen());
			try {
				printTicketAsycronous(pedidoVenta);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(principalForm, "Se guardara , pero hubo un error al imprimir:" + ex.getMessage(), "Imprimir", JOptionPane.ERROR_MESSAGE);
			}

			applicationLogic.persistCurrentPedidoVenta();

			pedidoVenta = applicationLogic.getSession().getPedidoVenta();
//			
//			new Thread() {
//				@Override
//				public void run() {
//					printTicketAsycronous(pedidoVenta);
//					st2 = System.currentTimeMillis();
//					logger.debug("\t->procederPedidoSucursal(): after Print done, T=" + (st2 - st1));
//				}
//			}.start();
			estadoInicial(true);
			st3 = System.currentTimeMillis();

			logger.debug("\t->procederPedido(): ok, estadoInicial, T=" + (st3 - st1));

			//logger.debug("\t->procederPedidoSucursal(): printTicketPedido");
			//applicationLogic.printTicketPedido(pedidoVenta,true,principalForm.getRecibimos().getText(),principalForm.getCambio().getText());            
			JOptionPane.showMessageDialog(principalForm, "VENTA REGISTRADA",
					"VENTA",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (BusinessException ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
					JOptionPane.ERROR_MESSAGE);
		} finally {
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}

	}

	private void printTicketAsycronous(PedidoVenta pedidoVenta) {
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {
			String recibimosTxt = principalForm.getRecibimos().getText();
			logger.debug("\t->printTicketAsycronous(): principalForm.getRecibimos().getText()=" + recibimosTxt + ", principalForm.getCambio().getText()=" + principalForm.getCambio().getText());
			String textRecived = principalForm.getRecibimos().getText();
			double cambio = calculaCambioContraRecibido(Double.parseDouble(textRecived));
			DecimalFormat df = new DecimalFormat("#####0.00");
			logger.debug("\t->printTicketAsycronous(): FIXED: cambio=" + cambio);
			applicationLogic.printTicketPedido(pedidoVenta, true, recibimosTxt, df.format(cambio));
		} catch (BusinessException ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
					JOptionPane.ERROR_MESSAGE);
		} finally {
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

	private void reprintTicketAsycronous(PedidoVenta pedidoVenta) {

		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {
			logger.debug("\t->reprintTicketAsycronous(): principalForm.getRecibimos().getText()=" + principalForm.getRecibimos().getText() + ", principalForm.getCambio().getText()=" + principalForm.getCambio().getText());
			applicationLogic.printTicketPedido(pedidoVenta, true, "0.00", "0.00");
		} catch (BusinessException ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					ApplicationInfo.getLocalizedMessage("LABEL_PROCED"),
					JOptionPane.ERROR_MESSAGE);
		} finally {
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}
	private Object[][] infoConcentradoReporte = null;
	private String fechaInicialReporte = null;
	private String fechaFinalReporte = null;
	private String usuarioReporte = null;
	private String totalReporte = null;

	void refreshVentasXTicket() {
		Object[][] infoConcentrado = null;
		Date fechaInicial = null;
		Date fechaFinal = null;
		if (principalForm.getPorFechaHoyRadioBtn().isSelected()) {
			fechaInicial = new Date();
			fechaFinal = new Date();
		} else {
			fechaInicial = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteInicio().getSelectedItem());
			fechaFinal = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteFin().getSelectedItem());
		}

		infoConcentrado = applicationLogic.concentradoVentasTicket(fechaInicial, fechaFinal);
		logger.info("----->refreshVentasXTicket :" + infoConcentrado.length);
		double total = 0.0;
		DecimalFormat df = new DecimalFormat("$###,###,##0.00");
		Double importe = null;
		for (Object[] row : infoConcentrado) {
			importe = (Double) row[6];
			total += importe;
			row[6] = df.format(importe);
		}
		String[] columnNames = {
			"Tipo",
			"Usuario",
			"Cliente",
			"Tipo de Pago",
			"Venta ID",
			"Fecha-hora",
			"Importe"};

		DefaultTableModel defaultTableModel = new DefaultTableModel(infoConcentrado, columnNames);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(defaultTableModel);
		principalForm.getTablaReporte().setModel(defaultTableModel);
		principalForm.getTablaReporte().setRowSorter(sorter);

		logger.info("----->refreshVentasXTicket : total" + df.format(total));
		principalForm.getTotalReporte().setText(df.format(total));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

		fechaInicialReporte = sdf.format(fechaInicial);
		fechaFinalReporte = sdf.format(fechaFinal);
		usuarioReporte = getApplicationLogic().getSession().getUsuario().getNombreCompleto();
		totalReporte = df.format(total);
		infoConcentradoReporte = infoConcentrado;
	}

	private void reimprimirTicket() {
		if (tipoReporte == 1) {
			int selectedRow = principalForm.getTablaReporte().getSelectedRow();
			if (selectedRow < 0) {
				JOptionPane.showMessageDialog(principalForm, "Tiene que seleciconar que Venta a imprimir", "Reimprimir", JOptionPane.WARNING_MESSAGE);
				principalForm.getTablaReporte().requestFocus();
				return;
			}
			int pedidoVentaId = new Integer(principalForm.getTablaReporte().getModel().getValueAt(selectedRow, 4).toString());
			logger.info("-->> Ok, reimprimirTicket:" + pedidoVentaId);
			try {
				PedidoVenta pv = null;
				pv = applicationLogic.findPedidoVenta(pedidoVentaId);
				logger.info("-->> PedidoVenta=" + pv);
				reprintTicketAsycronous(pv);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(principalForm, "Error al reimprimir:" + ex.getMessage(), "Reimprimir", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	void generarReportePDFYMostrarlo() {

		if (infoConcentradoReporte == null) {
			JOptionPane.showMessageDialog(principalForm, "Tiene que actualizar el reporte", "Generar PDF", JOptionPane.WARNING_MESSAGE);
			return;
		}

		String pdfPath = null;
		try {
			if (tipoReporte == 1) {
				pdfPath = GeneradorDeReportes.generateReporteVentasXTicket(infoConcentradoReporte, fechaInicialReporte, fechaFinalReporte, usuarioReporte, totalReporte);
			} else if (tipoReporte == 2) {
				pdfPath = GeneradorDeReportes.generateReporteVentasXUsuario(infoConcentradoReporte, fechaInicialReporte, fechaFinalReporte, usuarioReporte, totalReporte);
			} else if (tipoReporte == 3) {
				pdfPath = GeneradorDeReportes.generateReporteVentasXProducto(infoConcentradoReporte, fechaInicialReporte, fechaFinalReporte, usuarioReporte, totalReporte);
			}

		} catch (IOException ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(), "Generar PDF", JOptionPane.ERROR_MESSAGE);
		}

		if (pdfPath == null) {
			return;
		}

		try {
			Process exec = Runtime.getRuntime().exec(new String[]{"/usr/bin/xdg-open", pdfPath});
		} catch (Exception ex) {
			logger.error("Show PDF", ex);
			JOptionPane.showMessageDialog(principalForm, "Se genero el PDF en :" + pdfPath + ", \nPero sucedio un error al mostrarlo", "Mostrar PDF", JOptionPane.ERROR_MESSAGE);
		}

	}
	
	void actualizarPrecio() {
		
		final String precioCapturado = getPrincipalForm().getPrecioEncontrado().getText().trim();
		logger.debug("-->>actualizarPrecio:precioCapturado="+precioCapturado);
		
		if(precioCapturado.length()==0){
			return;
		}
		double precioNuevo;
		try {
			precioNuevo=Double.parseDouble(precioCapturado);
			
			if( precioNuevo > 1.0 ){
				logger.debug("->"+precioNuevo+" => ID="+productoEscaneado.getId());			
				applicationLogic.actualizaPrecio(productoEscaneado,applicationLogic.getSession().getAlmacen(),precioNuevo);
				JOptionPane.showMessageDialog(principalForm, "OK, actualizado", "Actualizar Precio", JOptionPane.INFORMATION_MESSAGE);
				principalForm.getCodigoBarrasBuscar().requestFocus();
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(), "Actualizar Precio", JOptionPane.ERROR_MESSAGE);
		}


	}

	void testPrinter() {
		try {
			applicationLogic.testPrinter();
		} catch (BusinessException ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					ApplicationInfo.getLocalizedMessage("APP_MENU_TEST_PRINT"),
					JOptionPane.ERROR_MESSAGE);
		}

	}

	void reiniciarPedido() {
		logger.debug("->reiniciarPedido()");
		int confirm = JOptionPane.showConfirmDialog(principalForm, ApplicationInfo.getLocalizedMessage("LABEL_RESET"),
				ApplicationInfo.getLocalizedMessage("RESET_PEDIDO_CONFIRMATION"),
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE);
		if (confirm == JOptionPane.YES_OPTION) {
			estadoInicial(true);
		}

	}

	void eliminarProductoDePedido() {
		logger.debug("->eliminarProductoDePedido()");
		int rowsBefore = principalForm.getDetallePedidoTable().getRowCount();
		int indexProdToDelete = principalForm.getDetallePedidoTable().getSelectedRow();
		logger.debug("\t==>>principalForm.getDetallePedidoTable().getSelectedRow=" + indexProdToDelete);
		if (indexProdToDelete >= 0) {
			applicationLogic.deleteProductoFromCurrentPedidoVenta(indexProdToDelete);
			updateDetallePedidoTable();
			if (rowsBefore == 1) {
				focusForCapture();
			}
		}
	}

	void createCliente() {
		logger.debug("->createCliente()");
		edicionClientesControl.crearCliente();
		if (edicionClientesControl.getExitStatus() == JOptionPane.YES_OPTION) {
			refreshClientesList();
		}

	}

	Cliente getSelectedCliente() {
		int cteSel = principalForm.getClientesTable().getSelectedRow();
		Cliente cs = null;
		if (cteSel != -1) {
			cs = ((ClientesTableModel) principalForm.getClientesTable().getModel()).getAt(cteSel);
		}
		return cs;
	}

	void editCliente() {
		Cliente c = getSelectedCliente();
		logger.debug("->editCliente():" + c);
		logger.debug("\t->editCliente(): problemas con CuentaBancariaCollection ??");
		edicionClientesControl.editarCliente(c);
		if (edicionClientesControl.getExitStatus() == JOptionPane.YES_OPTION) {
			refreshClientesList();
		}

	}

	void deleteCliente() {
		Cliente c = getSelectedCliente();
		logger.debug("->deleteCliente():" + c);

		try {
			int confirm = JOptionPane.showConfirmDialog(principalForm,
					ApplicationInfo.getLocalizedMessage("DLG_DELETE_CLIENTE_CONFIRMATION"),
					ApplicationInfo.getLocalizedMessage("DLG_DELETE_CLIENTE_MSG_TITLE"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (confirm == JOptionPane.YES_OPTION) {
				applicationLogic.removeCliente(c);
			}

			refreshClientesList();
		} catch (BusinessException ex) {
			JOptionPane.showMessageDialog(principalForm,
					ex.getMessage(),
					ex.getTitle(),
					JOptionPane.ERROR_MESSAGE);
		} catch (Exception ex) {
			logger.error("", ex);
			JOptionPane.showMessageDialog(principalForm,
					BusinessException.getLocalizedMessage("APP_LOGIC_CLIENTE_NOT_DELETED"),
					ApplicationInfo.getLocalizedMessage("DLG_DELETE_CLIENTE_MSG_TITLE"),
					JOptionPane.ERROR_MESSAGE);
		}

	}

	void exitByClick() {
		logger.debug("->exitByClick()");
		try {
			applicationLogic.exit();
			principalForm.setVisible(false);
			principalForm.dispose();
			logger.debug("->exitByClick(): OK normal exit!");
			System.exit(0);
		} catch (BusinessException ex) {
			int confirm = JOptionPane.showConfirmDialog(principalForm,
					ex.getMessage() + ApplicationInfo.getLocalizedMessage("EXIT_CONFIRMATION"),
					ApplicationInfo.getLocalizedMessage("APP_MENU_EXIT"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (confirm == JOptionPane.YES_OPTION) {
				principalForm.setVisible(false);
				principalForm.dispose();
				System.exit(0);
			}

		}
	}

	void exit() {
		logger.debug("->exit()");
		try {
			applicationLogic.exit();
			principalForm.setVisible(false);
			principalForm.dispose();
			logger.debug("->exit(): OK normal exit!");
			System.exit(0);

		} catch (BusinessException ex) {
			int confirm = JOptionPane.showConfirmDialog(principalForm,
					ex.getMessage() + ApplicationInfo.getLocalizedMessage("EXIT_CONFIRMATION"),
					ApplicationInfo.getLocalizedMessage("APP_MENU_EXIT"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (confirm == JOptionPane.YES_OPTION) {
				principalForm.setVisible(false);
				principalForm.dispose();
				System.exit(0);
			}

		}
	}

	void enviarPedidos() {
		logger.debug("->enviarPedidos()");

		if (applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().size() > 0) {
			JOptionPane.showMessageDialog(principalForm,
					ApplicationInfo.getLocalizedMessage("SAVE_BEFORE_SEND"),
					ApplicationInfo.getLocalizedMessage("SEND_ALL_PEDIDOS"),
					JOptionPane.QUESTION_MESSAGE);
		}

		try {
			applicationLogic.sendPedidosAndDelete(new ProgressProcessListener() {
				public void updateProgress(int prog, String msg) {
				}

				public int getProgress() {
					return 0;
				}
			});
			refreshVentasXTicket();

			JOptionPane.showMessageDialog(principalForm, ApplicationInfo.getLocalizedMessage("SEND_ALL_PEDIDOS_OK"),
					ApplicationInfo.getLocalizedMessage("SEND_ALL_PEDIDOS"),
					JOptionPane.WARNING_MESSAGE);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					ApplicationInfo.getLocalizedMessage("SEND_ALL_PEDIDOS"),
					JOptionPane.ERROR_MESSAGE);
		} finally {
			//principalForm.getLabel3().setText("");
		}
	}

	private void verVentasXUsuario() {
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		refreshRangoDeFechas();
		refreshVentasXUsuario();
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		toFrontPanel("visorDeReportes");
		principalForm.getTablaReporte().requestFocus();
	}

	private void refreshRangoDeFechas() {
		List<Date> rangoDeFechas = applicationLogic.getRangoFechasDeVentas();
		principalForm.getFechaReporteInicio().setModel(new FechaComboBoxModel(rangoDeFechas));
		principalForm.getFechaReporteFin().setModel(new FechaComboBoxModel(rangoDeFechas));
	}

	private void refreshVentasXUsuario() {
		Object[][] infoConcentrado = null;
		Date fechaInicial = null;
		Date fechaFinal = null;
		if (principalForm.getPorFechaHoyRadioBtn().isSelected()) {
			fechaInicial = new Date();
			fechaFinal = new Date();
		} else {
			fechaInicial = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteInicio().getSelectedItem());
			fechaFinal = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteFin().getSelectedItem());
		}

		infoConcentrado = applicationLogic.concentradoVentasUsuario(fechaInicial, fechaFinal);
		logger.info("----->verConcentradoVentsUsuario :" + infoConcentrado.length);
		double total = 0.0;
		DecimalFormat df = new DecimalFormat("$###,###,##0.00");
		Double importe = null;
		for (Object[] row : infoConcentrado) {
			importe = (Double) row[4];
			total += importe;
			row[4] = df.format(importe);
		}

		String[] columnNames = {
			"Nombre Completo",
			"Usuario",
			"Tipo Almacen",
			"Fecha",
			"Importe"};
		DefaultTableModel defaultTableModel = new DefaultTableModel(infoConcentrado, columnNames);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(defaultTableModel);
		principalForm.getTablaReporte().setModel(defaultTableModel);
		principalForm.getTablaReporte().setRowSorter(sorter);
		logger.info("----->refreshVentasXUsuario : total" + df.format(total));
		principalForm.getTotalReporte().setText(df.format(total));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

		fechaInicialReporte = sdf.format(fechaInicial);
		fechaFinalReporte = sdf.format(fechaFinal);
		usuarioReporte = getApplicationLogic().getSession().getUsuario().getNombreCompleto();
		totalReporte = df.format(total);
		infoConcentradoReporte = infoConcentrado;
	}

	private void verVentasXProducto() {
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		refreshRangoDeFechas();
		refreshVentasXProducto();
		principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		toFrontPanel("visorDeReportes");
		principalForm.getTablaReporte().requestFocus();
	}

	private void refreshVentasXProducto() {
		Object[][] infoConcentrado = null;
		Date fechaInicial = null;
		Date fechaFinal = null;
		if (principalForm.getPorFechaHoyRadioBtn().isSelected()) {
			fechaInicial = new Date();
			fechaFinal = new Date();
		} else {
			fechaInicial = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteInicio().getSelectedItem());
			fechaFinal = FechaComboBoxModel.parseOwnElement((String) principalForm.getFechaReporteFin().getSelectedItem());
		}

		infoConcentrado = applicationLogic.concentradoVentasProducto(fechaInicial, fechaFinal);
		logger.info("----->refreshVentasXProducto :" + infoConcentrado.length);
		double total = 0.0;
		DecimalFormat df = new DecimalFormat("$###,###,##0.00");
		Double importe = null;
		for (Object[] row : infoConcentrado) {
			importe = (Double) row[5];
			total += importe;
			row[5] = df.format(importe);
		}
		String[] columnNames = {
			"Cantidad",
			"CODIGO",
			"Producto",
			"Presentacion",
			"Tipo Venta",
			"Importe"};
		DefaultTableModel defaultTableModel = new DefaultTableModel(infoConcentrado, columnNames);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(defaultTableModel);
		principalForm.getTablaReporte().setModel(defaultTableModel);
		principalForm.getTablaReporte().setRowSorter(sorter);
		logger.info("----->refreshVentasXProducto : total" + df.format(total));
		principalForm.getTotalReporte().setText(df.format(total));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

		fechaInicialReporte = sdf.format(fechaInicial);
		fechaFinalReporte = sdf.format(fechaFinal);
		usuarioReporte = getApplicationLogic().getSession().getUsuario().getNombreCompleto();
		totalReporte = df.format(total);
		infoConcentradoReporte = infoConcentrado;
	}

	public void estadoInicial(boolean paraSucursal) {
		logger.debug("\t->Estado Inicial");
		applicationLogic.startNewPedidoVentaSession();

		principalForm.getLabel1().setText(ApplicationInfo.getLocalizedMessage("USUARIOACTUAL")
				+ applicationLogic.getSession().getUsuario().getNombreCompleto());
		principalForm.getLabel3().setText("SUCURSAL: " + applicationLogic.getSession().getSucursal().getId());
		principalForm.getRecibimos().setText("");
		principalForm.getCambio().setText("");

		//principalForm.setExtendedState(principalForm.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		////principalForm.getDeleteItemPedidoMenu().setEnabled(false);
		if (paraSucursal) {
			ClienteComboBoxModel clienteCBM = (ClienteComboBoxModel) principalForm.getCliente().getModel();
			int nc = clienteCBM.getSize();
			int scf = 0;
			for (int cci = 1; cci < nc; cci++) {
				if (((ClienteItemList) clienteCBM.getElementAt(cci)).getCliente().getId().intValue() == 1) {
					scf = cci;
				}
			}
			principalForm.getCliente().setSelectedIndex(scf);
			principalForm.getFormaDePago().setSelectedIndex(1);
		} else {
			principalForm.getCliente().setSelectedIndex(0);
			principalForm.getFormaDePago().setSelectedIndex(0);
		}
		verifyAllSelections();

		PedidoVentaDetalleTableModel pedidoVentaDetalleTableModel = new PedidoVentaDetalleTableModel(
				applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection());

		pedidoVentaDetalleTableModel.setPrincipalControl(this);
		pedidoVentaDetalleTableModel.setApplicationLogic(applicationLogic);
		//======================================================================
		principalForm.getDetallePedidoTable().setModel(pedidoVentaDetalleTableModel);
		final PedidoVentaDetalle pedidoVentaDetalleZero = new PedidoVentaDetalle();
		pedidoVentaDetalleZero.setCantidad(0);
		pedidoVentaDetalleZero.setPrecioVenta(0.0);
		pedidoVentaDetalleZero.setProducto(new Producto(0));

		applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().add(pedidoVentaDetalleZero);

		TableColumn column = null;
		TableColumnModel columnModel = principalForm.getDetallePedidoTable().getColumnModel();
		int tableWidth = principalForm.getDetallePedidoTable().getWidth();
		for (int i = 0; i < 5; i++) {
			column = columnModel.getColumn(i);
			if (i == 2) {
				column.setPreferredWidth((tableWidth * 30) / 100); //third column is bigger
			} else {
				column.setPreferredWidth((tableWidth * 10) / 100);
			}
		}
		principalForm.getDetallePedidoTable().getColumnModel().getColumn(4).
				setCellEditor(new DiscountDVPCellEditor());

		principalForm.getTotal().setText("$ 0.00");
		principalForm.getSubtotal().setText("$ 0.00");

		principalForm.getDetallePedidoTable().updateUI();

		applicationLogic.getSession().getPedidoVenta().getPedidoVentaDetalleCollection().clear();

		principalForm.getCliente().updateUI();
		principalForm.getFormaDePago().updateUI();

		principalForm.getDetallePedidoTable().updateUI();

		principalForm.getChangePedidoNormal().setEnabled(true);
		principalForm.getChangePedidoOportunidad().setEnabled(true);
		//======================================================================
		toFrontPanel("visorDePedidoActual");
		if (!principalForm.isVisible()) {
			principalForm.setVisible(true);
		}
		focusForCapture();

	}

	void generaTarjetasDePrecios() {
		try {
//			if(!applicationLogic.getSession().getUsuario().getNombreCompleto().equals("root")) {
//				throw new Exception("Solo un administrador puede generar las listas");
//			}
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			GeneradorTarjetasPrecios.generateTicket(applicationLogic.getProductosForPrinting(), applicationLogic.getSession().getAlmacen());

		} catch (Exception ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					"Generar Tarjetas de precios",
					JOptionPane.ERROR_MESSAGE);
		} finally {
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

	void generaTarjetasDePreciosProductoActual() {
		try {
//			if(!applicationLogic.getSession().getUsuario().getNombreCompleto().equals("root")) {
//				throw new Exception("Solo un administrador puede generar las listas");
//			}
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			GeneradorTarjetasPrecios.generateTicket(productoEscaneado, applicationLogic.getSession().getAlmacen());

		} catch (Exception ex) {
			JOptionPane.showMessageDialog(principalForm, ex.getMessage(),
					"Generar Tarjetas de precios",
					JOptionPane.ERROR_MESSAGE);
		} finally {
			principalForm.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

	void updateProductoSelected(ProductoFastDisplayModel selDisplayProducto, JTextField cantidadPedidaTF) {
		logger.debug("\t->itemStateChanged:" + selDisplayProducto + ", n Prod. surtidos=" + (listProductosSinSurtido != null ? listProductosSinSurtido.size() : null));

		if (selDisplayProducto != null) {
			applicationLogic.getSession().setProductoBuscadoActual(selDisplayProducto.getProducto());

			getPrincipalForm().getProductoCBEncontrados().setEnabled(true);

			cantidadPedidaTF.setText("1");
			cantidadPedidaTF.setSelectionStart(0);
			cantidadPedidaTF.setSelectionEnd(1);
			cantidadPedidaTF.setEnabled(true);
		} else {
			applicationLogic.getSession().setProductoBuscadoActual(null);
			getPrincipalForm().getProductoCBEncontrados().setEnabled(false);
			cantidadPedidaTF.setText("");
			cantidadPedidaTF.setEnabled(false);
		}

	}

	void updateProductosFound() {
		logger.debug("->updateProductosFound(): currentTextToSearch=" + currentTextToSearch);
		getPrincipalForm().getProductoCBEncontrados().setModel(new DefaultComboBoxModel(
				applicationLogic.getProducto4Display(currentTextToSearch)));
		getPrincipalForm().getProductoCBEncontrados().updateUI();
		if (getPrincipalForm().getProductoCBEncontrados().getModel().getSize() > 0) {
			updateProductoSelected(
					(ProductoFastDisplayModel) getPrincipalForm().getProductoCBEncontrados().getModel().getElementAt(0),
					getPrincipalForm().getCantidadPedida());
			getPrincipalForm().getProductoCBEncontrados().setPopupVisible(true);
		} else {
			updateProductoSelected(null, getPrincipalForm().getCantidadPedida());
			getPrincipalForm().getProductoCBEncontrados().setPopupVisible(false);
		}

	}

	private void checadorDePrecios() {
		limpiarResultadoBusqueda();
		toFrontPanel("visorChecadorDePrecios");
		principalForm.getCodigoBarrasBuscar().requestFocus();
	}
	private Producto productoEscaneado = null;
	private void checarPrecio(String codigoEscaneado) {

		try {
			productoEscaneado = applicationLogic.findProductoByCodigoDeBarras(codigoEscaneado);
			principalForm.getNombreDescripcionEncontrado().setText(productoEscaneado.getNombre() + " (" + productoEscaneado.getPresentacion() + ")");
			final Collection<AlmacenProducto> almacenProductoCollection = productoEscaneado.getAlmacenProductoCollection();
			String precioAlmacen = null;
			String existenciaActual = null;
			for (AlmacenProducto ap : almacenProductoCollection) {
				if (ap.getAlmacen().getId().intValue() == applicationLogic.getSession().getAlmacen().getId().intValue()) {
					precioAlmacen = dfChecadorPrecios.format(ap.getPrecioVenta());
					if (ap.getCantidadActual() > 0) {
						existenciaActual = "Existencia: " + String.valueOf(ap.getCantidadActual()) + " Pzas.";
					} else {
						existenciaActual = "Existencia: NO SINCRONIZADA.";
					}
				}
			}

			if (precioAlmacen == null || existenciaActual == null) {
				precioAlmacen = "";
				existenciaActual = "NO HAY EXISTENCIA";
			}
			principalForm.getCodigoBarrasBuscar().setText("");
			principalForm.getPrecioEncontrado().setText(precioAlmacen);
			principalForm.getCodigoBarrasEncontrado().setText(productoEscaneado.getContenido() + " " + productoEscaneado.getUnidadMedida());
			principalForm.getExistenciaEncontrada().setText(existenciaActual);
		} catch (Exception ex) {
			limpiarResultadoBusqueda();
		}
	}

	/**
	 * @return the principalForm
	 */
	public PrincipalForm getPrincipalForm() {
		return principalForm;
	}
}