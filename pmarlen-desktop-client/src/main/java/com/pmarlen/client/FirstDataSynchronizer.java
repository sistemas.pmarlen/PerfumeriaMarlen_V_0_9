/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client;

import com.pmarlen.businesslogic.exception.UpdateBugFixingException;
import com.pmarlen.businesslogic.exception.UpdateInminentException;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.*;
import com.pmarlen.model.controller.PersistEntityWithTransactionDAO;
import com.pmarlen.wscommons.services.GetListDataBusiness;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author alfred
 */
@Repository("firstDataSynchronizer")
public class FirstDataSynchronizer {

    private Logger logger;

    private PersistEntityWithTransactionDAO persistEntityWithTransactionDAO;

    private SynchronizationWithServerRegistryController synchronizationWithServerRegistryController;

        
    public FirstDataSynchronizer() {
        logger = LoggerFactory.getLogger(FirstDataSynchronizer.class);
    }

    /**
     * @param persistEntityWithTransactionDAO the persistEntityWithTransactionDAO to set
     */
    @Autowired
    public void setPersistEntityWithTransactionDAO(PersistEntityWithTransactionDAO persistEntityWithTransactionDAO) {
        this.persistEntityWithTransactionDAO = persistEntityWithTransactionDAO;
    }

        /**
     * @param synchronizationWithServerRegistryController the synchronizationWithServerRegistryController to set
     */
    @Autowired
    public void setSynchronizationWithServerRegistryController(SynchronizationWithServerRegistryController synchronizationWithServerRegistryController) {
        this.synchronizationWithServerRegistryController = synchronizationWithServerRegistryController;
    }

    public void checkServerVersion() throws IllegalStateException,UpdateInminentException,UpdateBugFixingException, BusinessException {

        String serverVersion = null;
		logger.debug("-> checkServerVersion:Constants.getServerVersion()="+Constants.getServerVersion());
        GetListDataBusiness getListDataBusinessServiceClient = WebServiceConnectionConfig.getInstance().getGetListDataBusiness();
        logger.debug("-> checkServerVersion: WS Stub getListDataBusinessServiceClient=" + getListDataBusinessServiceClient);
        serverVersion = getListDataBusinessServiceClient.getServerVersion();

        logger.debug("\t--->>checkServerVersion():serverVersion=" + serverVersion);

        String[] serverVersionPart = serverVersion.split("\\.");
        String[] mainVersionPart = ApplicationInfo.getInstance().getVersion().split("\\.");
        if (!serverVersionPart[0].equalsIgnoreCase(mainVersionPart[0])){
			throw new IllegalStateException("No se tiene una version compatible con servidor.\n"
                    + " Instale una version compatible con la Version:" + serverVersionPart[0] + "." + serverVersionPart[1] + "@xxx");
		} else if(Long.parseLong(serverVersionPart[1]) > Long.parseLong(mainVersionPart[1]) ) {
            throw new UpdateInminentException("Inminent Update "+ApplicationInfo.getInstance().getVersion()+" -> "+serverVersion);
        } else if(Long.parseLong(serverVersionPart[2]) > Long.parseLong(mainVersionPart[2])) {
            throw new UpdateBugFixingException("Actualización de versión "+ApplicationInfo.getInstance().getVersion()+" a versión "+serverVersion);
        } else if(Long.parseLong(serverVersionPart[2]) > Long.parseLong(mainVersionPart[2])) {
            logger.info("Dynamic patch :P !!");
        }
		logger.debug("<----checkServerVersion():OK");

    }

    public void firstSyncronization(ProgressProcessListener progressListener) throws Exception {
        logger.debug("====================>> firstSyncronization(): just on Master Host !!");

        progressListener.updateProgress(30, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_CREATING_WS"));
        GetListDataBusiness getListDataBusinessServiceClient = WebServiceConnectionConfig.getInstance().getGetListDataBusiness();

        try {
            logger.debug("firstSyncronization():delete all objs");
            progressListener.updateProgress(31, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_DELETE_ALL_OBJECTESS"));
            persistEntityWithTransactionDAO.deleteAllObjects();
			
            // =====================================================================
			long t0= System.currentTimeMillis();
            logger.info("==>> 5.1:Invoking service getAllDataPackCompressed()");

			byte[] bytes = getListDataBusinessServiceClient.getAllDataPackCompressed();
			long t1= System.currentTimeMillis();
			logger.info("==>> 5.2: T="+(t1-t0)+" millisecs, bytes length="+bytes.length);

			ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
			GZIPInputStream gzipIn = new GZIPInputStream(bais);
			ObjectInputStream objectIn = new ObjectInputStream(gzipIn);
			int listIndex=0;
			
			List allObjects=(List)objectIn.readObject();
			List<Usuario> listaUsuarios = (List<Usuario>)allObjects.get(listIndex++);

            for(Usuario x: listaUsuarios) {
                //logger.info("\t-> Usuario:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			long t2= System.currentTimeMillis();
			logger.info("==>> 5.3: T="+(t2-t1)+" millisecs, listaUsuarios.size="+listaUsuarios.size());
			
            List<Industria> listaIndustria = (List<Industria>)allObjects.get(listIndex++);

            for(Industria x: listaIndustria) {
                //logger.info("\t-> Industria:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			long t3= System.currentTimeMillis();
			logger.info("==>> 5.4: T="+(t3-t2)+" millisecs, listaIndustria.size="+listaIndustria.size());
			

            List<Linea> listaLinea = (List<Linea>)allObjects.get(listIndex++);

            for(Linea x: listaLinea) {
                //logger.info("\t-> Linea:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			long t4= System.currentTimeMillis();
			logger.info("==>> 5.5: T="+(t4-t3)+" millisecs, listaLinea.size="+listaLinea.size());

            List<Marca> listaMarca = (List<Marca>)allObjects.get(listIndex++);

            for(Marca x: listaMarca) {
                //logger.info("\t-> Marca:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			long t5= System.currentTimeMillis();
			logger.info("==>> 5.6: T="+(t5-t4)+" millisecs, listaMarca.size="+listaMarca.size());

            List<Producto> listaProducto = (List<Producto>)allObjects.get(listIndex++);

            for(Producto x: listaProducto) {
                //logger.info("\t-> Producto:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			long t6= System.currentTimeMillis();
			logger.info("==>> 5.7: T="+(t6-t5)+" millisecs, listaProducto.size="+listaProducto.size());
			
            List<Multimedio> listaMultimedio = (List<Multimedio>)allObjects.get(listIndex++);
            if(listaMultimedio != null) {
				for(Multimedio x: listaMultimedio) {
					//logger.info("\t-> Multimedio:RutaContenido="+x.getRutaContenido()+", NombreArchivo=" + x.getNombreArchivo()+", MimeType="+x.getMimeType()+", Size="+x.getSizeBytes());                    
				}
			}
			long t7= System.currentTimeMillis();
			logger.info("==>> 5.8: T="+(t7-t6)+" millisecs, listaMultimedio.size="+listaMultimedio.size());
			
			List<AlmacenProducto> listaAlmacenProducto = (List<AlmacenProducto>)allObjects.get(listIndex++);
            if(listaAlmacenProducto != null) {
				for(AlmacenProducto x: listaAlmacenProducto) {
					//logger.info("\t-> AlmacenProducto:"+ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
				}
			}
			long t8= System.currentTimeMillis();
			logger.info("==>> 5.9: T="+(t8-t7)+" millisecs, listaAlmacenProducto.size="+listaAlmacenProducto.size());

            List<FormaDePago> listaFormaDePago = (List<FormaDePago>)allObjects.get(listIndex++);
            if(listaFormaDePago != null){
                for(FormaDePago x: listaFormaDePago) {
                    //logger.info("\t-> FormaDePago:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
                }
            }
			long t9= System.currentTimeMillis();
			logger.info("==>> 5.10: T="+(t9-t8)+" millisecs, listaFormaDePago.size="+listaFormaDePago.size());

            List<Estado> listaEstado = (List<Estado>)allObjects.get(listIndex++);
            if(listaEstado != null){
                for(Estado x: listaEstado) {
                    //logger.info("\t-> Estado:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
                }
            }
			long t10= System.currentTimeMillis();
			logger.info("==>> 5.11: T="+(t10-t9)+" millisecs, listaEstado.size="+listaEstado.size());

			List<Sucursal> listaSucursal = (List<Sucursal>)allObjects.get(listIndex++);
			
            if(listaSucursal != null){
                for(Sucursal x: listaSucursal) {
                    //logger.info("\t-> Sucursal:" + x.getId());
					Collection<Usuario> usuarioCollection = x.getUsuarioCollection();
					
					if(usuarioCollection != null) {
						for(Usuario u: usuarioCollection){
							//logger.info("\t\t-> Usuario:" + u.getUsuarioId()); 
						}
					}
					
					Collection<Almacen> almacenCollection = x.getAlmacenCollection();
					if(almacenCollection != null) {
						for(Almacen almacen: almacenCollection){
							//logger.info("\t\t-> Almacen:" + almacen.getId());
							Collection<AlmacenProducto> almacenProductoCollection = almacen.getAlmacenProductoCollection();
							if(almacenProductoCollection != null) {
								for(AlmacenProducto ap: almacenProductoCollection){
									//logger.info("\t\t\t-> Producto:"+ap.getProducto().getId()+" x "+ap.getCantidadActual()); 
								}
							}
						}
					}
                }
            }
			long t11= System.currentTimeMillis();
			logger.info("==>> 5.12: T="+(t11-t10)+" millisecs, listaSucursal.size="+listaSucursal.size());

            List<Cliente> listaCliente = (List<Cliente>)allObjects.get(listIndex++);
            if(listaCliente != null){
                for(Cliente x: listaCliente) {
                    //logger.info("\t-> Cliente:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
                }
            }
			long t12= System.currentTimeMillis();
			logger.info("==>> 5.13: T="+(t12-t11)+" millisecs, listaCliente.size="+listaCliente.size());
			
			logger.info("\t==>> 6: T="+(t1-t0)+" + "+(t12-t1)+" = "+(t12-t0)+" millisecs, all process");
			
			
			
			// =====================================================================
			progressListener.updateProgress(33, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_SUCURSALLIST"));
            List<Sucursal> resultSucursalList = listaSucursal;
            
			progressListener.updateProgress(35, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_SUCURSALLIST"));
            persistEntityWithTransactionDAO.inicializarSucursal(resultSucursalList, progressListener);
            
            // =====================================================================
			
            progressListener.updateProgress(36, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_USUARIOLIST"));
            List<Usuario> usuarioReceivedList = listaUsuarios;
			
			progressListener.updateProgress(38, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_USUARIOLIST"));
            logger.debug("firstSyncronization():WebService:getUsuarioList():Result = " + usuarioReceivedList);
            persistEntityWithTransactionDAO.inicializarUsuarios(usuarioReceivedList, progressListener);
            usuarioReceivedList = null;
			
            // =====================================================================
            progressListener.updateProgress(40, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_LINEALIST"));
            List<Linea> lineaReceivedList = listaLinea;
			
			logger.debug("firstSyncronization():WebService:getLineaList():Result = " + lineaReceivedList);
            progressListener.updateProgress(43, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_LINEALIST"));
            persistEntityWithTransactionDAO.inicializarLinea(lineaReceivedList, progressListener);
            lineaReceivedList = null;
            // =====================================================================
            progressListener.updateProgress(45, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_INDUSTRIALIST"));
            List<Industria> industriaRecivedList = listaIndustria;
            
			logger.debug("firstSyncronization():WebService:getIndustriaList():Result = " + industriaRecivedList);
            progressListener.updateProgress(48, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_INDUSTRIALIST"));
            persistEntityWithTransactionDAO.inicializarIndustrias(industriaRecivedList, progressListener);
            industriaRecivedList = null;
            // =====================================================================
            progressListener.updateProgress(50, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_MARCALIST"));
            List<Marca> marcaReceivedList = listaMarca;
			
            logger.debug("firstSyncronization():WebService:getMarcaList():Result = " + marcaReceivedList);
            progressListener.updateProgress(53, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_MARCALIST"));
            persistEntityWithTransactionDAO.inicializarMarcas(marcaReceivedList, progressListener);
            marcaReceivedList = null;
            // =====================================================================            
            progressListener.updateProgress(55, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_PRODUCTOLIST"));
            List<Producto> productoReceivedList = listaProducto;
            
			logger.debug("firstSyncronization():WebService:getProductoList():Result size = " + productoReceivedList.size());
            progressListener.updateProgress(56, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_PRODUCTOLIST"));
            persistEntityWithTransactionDAO.inicializarProductos(productoReceivedList, progressListener);
            productoReceivedList = null;
			// =====================================================================            
            progressListener.updateProgress(58, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_ALMACENPRODUCTOLIST"));
            List<AlmacenProducto> almacenProductoReceivedList = listaAlmacenProducto;
            
			logger.debug("firstSyncronization():WebService:getAlmacenProductoList():Result size = " + almacenProductoReceivedList.size());
            progressListener.updateProgress(56, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_ALMACENPRODUCTOLIST"));
            persistEntityWithTransactionDAO.inicializarAlmacenProductos(almacenProductoReceivedList, progressListener);
            almacenProductoReceivedList = null;
            // =====================================================================

            progressListener.updateProgress(60, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_FORMADEPAGOLIST"));
            List<FormaDePago> formaDePagoReceivedList = listaFormaDePago;
            
			logger.debug("firstSyncronization():WebService:getFormaDePagoList():Result size = " + formaDePagoReceivedList.size());
            progressListener.updateProgress(63, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_FORMADEPAGOLIST"));
            persistEntityWithTransactionDAO.inicializarFormaDePago(formaDePagoReceivedList, progressListener);
            formaDePagoReceivedList = null;
            // =====================================================================
            progressListener.updateProgress(65, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_ESTADOLIST"));
            List<Estado> estadoReceivedList = listaEstado;
            
			logger.debug("firstSyncronization():WebService:getEstadoList():Result size = " + estadoReceivedList.size());
            progressListener.updateProgress(68, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_ESTADOLIST"));
            persistEntityWithTransactionDAO.inicializarEstado(estadoReceivedList, progressListener);
            estadoReceivedList = null;
            // =====================================================================

            progressListener.updateProgress(70, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_CLIENTELIST"));
            List<Cliente> resultClienteList = listaCliente;
            
//			logger.debug("firstSyncronization():WebService:getClienteList():Result size = " + resultClienteList.size());
            progressListener.updateProgress(73, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_CLIENTELIST"));
            persistEntityWithTransactionDAO.inicializarCliente(resultClienteList, progressListener);
            resultClienteList = null;
            // =====================================================================
            progressListener.updateProgress(74, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_GET_MULTIMEDIOLIST"));
            List<Multimedio> resultMultimedioList = listaMultimedio;
            
			progressListener.updateProgress(75, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_WS_UPDATE_MULTIMEDIOLIST"));
            persistEntityWithTransactionDAO.inicializarMultimedio(resultMultimedioList, progressListener);
            resultMultimedioList = null;
            // =====================================================================
            progressListener.updateProgress(80, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_UPDATE_LASTSYNC"));
			
            synchronizationWithServerRegistryController.updateLastSyncronization();

            //persistEntityWithTransactionDAO.reloadEMF();

            logger.debug("firstSyncronization():end OK");
            progressListener.updateProgress(92, ApplicationInfo.getLocalizedMessage("APPLICATION_STARTER_OK"));

        } catch (Exception ex) {
            throw ex;
        } finally {
            getListDataBusinessServiceClient = null;
            //logger.debug("firstSyncronization():gc");
            //System.gc();
        }
    }

    boolean needSyncronization() {
        return synchronizationWithServerRegistryController.needSyncronization();
    }

    public boolean isSyncronizatedInThisSession() {
        return synchronizationWithServerRegistryController.isSyncronizatedInThisSession();
    }

}
