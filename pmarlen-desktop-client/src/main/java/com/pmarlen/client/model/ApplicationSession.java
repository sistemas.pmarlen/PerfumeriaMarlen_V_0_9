/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pmarlen.client.model;

import com.pmarlen.model.beans.Almacen;
import com.pmarlen.model.beans.Marca;
import com.pmarlen.model.beans.PedidoVenta;
import com.pmarlen.model.beans.Producto;
import com.pmarlen.model.beans.Sucursal;
import com.pmarlen.model.beans.Usuario;
import java.util.Properties;
import org.springframework.stereotype.Component;


/**
 *
 * @author alfred
 */
@Component("applicationSession")
public class ApplicationSession {
    
    private Usuario usuario;
	
	private String realPassword;
	
	private Sucursal sucursal;
	
	private Almacen almacen;

    private PedidoVenta pedidoVenta;

    private Marca marcaPorLinea;

    private Marca marcaPorIndustria;

    private Producto productoBuscadoActual;
    
	//private Properties preferences;

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the pedidoVenta
     */
    public PedidoVenta getPedidoVenta() {
        return pedidoVenta;
    }

    /**
     * @param pedidoVenta the pedidoVenta to set
     */
    public void setPedidoVenta(PedidoVenta pedidoVenta) {
        this.pedidoVenta = pedidoVenta;
    }

    /**
     * @return the marcaPorIndustria
     */
    public Marca getMarcaPorIndustria() {
        return marcaPorIndustria;
    }

    /**
     * @param marcaPorIndustria the marcaPorIndustria to set
     */
    public void setMarcaPorIndustria(Marca marcaPorIndustria) {
        this.marcaPorIndustria = marcaPorIndustria;
    }

    /**
     * @return the marcaPorLinea
     */
    public Marca getMarcaPorLinea() {
        return marcaPorLinea;
    }

    /**
     * @param marcaPorLinea the marcaPorLinea to set
     */
    public void setMarcaPorLinea(Marca marcaPorLinea) {
        this.marcaPorLinea = marcaPorLinea;
    }

    /**
     * @return the productoBuscadoActual
     */
    public Producto getProductoBuscadoActual() {
        return productoBuscadoActual;
    }

    /**
     * @param productoBuscadoActual the productoBuscadoActual to set
     */
    public void setProductoBuscadoActual(Producto productoBuscadoActual) {
        this.productoBuscadoActual = productoBuscadoActual;
    }

	/**
	 * @return the sucursal
	 */
	public Sucursal getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the almacen
	 */
	public Almacen getAlmacen() {
		return almacen;
	}

	/**
	 * @param almacen the almacen to set
	 */
	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}

	public void setRealPassword(String password) {
		this.realPassword = password;
	}
	
	public String getRealPassword() {
		return this.realPassword;
	}	
}
