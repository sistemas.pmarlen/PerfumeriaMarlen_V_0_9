/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.ticketprinter;

import com.pmarlen.client.ApplicationLogic;
import com.pmarlen.model.beans.PedidoVenta;
import com.pmarlen.model.beans.PedidoVentaDetalle;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alfredo
 */
public class TicketPOSTermalPrinter implements TicketPrinteService {
	private ApplicationLogic applicationLogic;
	
	private Logger logger;
	
	public TicketPOSTermalPrinter(){
		logger = LoggerFactory.getLogger(TicketPOSTermalPrinter.class);
		logger.debug("->ApplicationLogic, created");
	}

	
	@Override
	public Object generateTicket(PedidoVenta pedidoVenta, HashMap<String, String> extraInformation) throws IOException {
		logger.debug("-->>>generateTicket: pedidoVenta="+pedidoVenta.getId());
		logger.debug("-->>>generateTicket: usuario="+pedidoVenta.getUsuario());

		String fileGenarated = null;
		String reportPath,compiledReportPath;
		JasperPrint jasperPrint = null;
		reportPath			= "/ticket_layout/TicketDesign_SUC_2.jrxml";
		compiledReportPath	= "/ticket_layout/TicketDesign_SUC_2.jasper";

		Collection<Map<String, ?>> col = new ArrayList<Map<String, ?>>();
		DecimalFormat df = new DecimalFormat("$###,###,###,##0.00");
		DecimalFormat dfEnt = new DecimalFormat("###########0.00");
		DecimalFormat dfBC = new DecimalFormat("0000000000000");
		//logger.debug("Ok, jrxml loaded");
		double p = 0.0;
		double sim = 0.0;
		int n;
		int numReg = 0;
		Random rand = new Random(System.currentTimeMillis());
		Collection<PedidoVentaDetalle> pedidoVentaDetalleCollection = pedidoVenta.getPedidoVentaDetalleCollection();
		numReg = pedidoVentaDetalleCollection.size();
		for (PedidoVentaDetalle pvd : pedidoVentaDetalleCollection) {


			Map<String, Object> vals = new HashMap<String, Object>();

			n = pvd.getCantidad();

			vals.put("cantidad", n);
			vals.put("codigoBarras", pvd.getProducto().getCodigoBarras());
			vals.put("descripcion", pvd.getProducto().getNombre() + "/" + pvd.getProducto().getPresentacion());
			p = pvd.getPrecioVenta();
			
			sim += n * p;

			vals.put("precio", df.format(p));
			vals.put("importe", df.format(n*p));

			col.add(vals);
		}
		double st = sim / 1.16;
		double d = pedidoVenta.getDescuentoAplicado() != null ? pedidoVenta.getDescuentoAplicado().doubleValue() : 0.0;

		JRDataSource beanColDataSource = new JRMapCollectionDataSource(col);
		logger.debug("Ok, JRDataSource created");

		Map parameters = new HashMap();

		SimpleDateFormat sdf_f1 = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdf_h1 = new SimpleDateFormat("HH:mm");


		Date fechaReporte = new Date();
		parameters.put("codigoPedido", new Integer(987654112));
		parameters.put("fecha", sdf_f1.format(fechaReporte));
		parameters.put("hora", sdf_h1.format(fechaReporte));
		parameters.put("usuario", pedidoVenta.getUsuario().getNombreCompleto());
		parameters.put("cliente", pedidoVenta.getCliente().getRazonSocial());
		parameters.put("rfc", pedidoVenta.getCliente().getRfc());
		double total = sim;
		
		String recibimosOriginal = extraInformation.get("recibimos").toString();
		String recibimos         = recibimosOriginal;
		if(recibimos!=null && recibimos.trim().length()>0){
			recibimos = df.format(Double.parseDouble(recibimos));
		}
		parameters.put("recibimos", recibimos);
		String suCambio = extraInformation.get("cambio").toString();
		if(suCambio == null || suCambio.trim().length()==0){
			suCambio = df.format(Double.parseDouble(recibimosOriginal) - total);
		}
		parameters.put("cambio", suCambio);

		parameters.put("direccion",
				pedidoVenta.getCliente().getCalle() + ", "
				+ pedidoVenta.getCliente().getNumExterior() + ", "
				+ pedidoVenta.getCliente().getNumInterior() + ", "
				+ pedidoVenta.getCliente().getPoblacion().getTipoAsentamiento() + " " + pedidoVenta.getCliente().getPoblacion().getNombre() + ", "
				+ pedidoVenta.getCliente().getPoblacion().getMunicipioODelegacion() + ", "
				+ pedidoVenta.getCliente().getPoblacion().getEntidadFederativa() + ", C.P."
				+ pedidoVenta.getCliente().getPoblacion().getCodigoPostal());
		parameters.put("condiciones", pedidoVenta.getFormaDePago().getDescripcion());
		parameters.put("subtotal", df.format(st));
		parameters.put("iva", df.format(st * 0.16));
		parameters.put("descuento", df.format(sim * d));

		
		parameters.put("total", df.format(total));

		String intDecParts[] = dfEnt.format(total).split("\\.");

		String letrasParteEntera = NumeroCastellano.numeroACastellano(Long.parseLong(intDecParts[0])).trim();
		//String letrasParteDecimal = NumeroCastellano.numeroACastellano(Long.parseLong(intDecParts[1])).trim();

		parameters.put("importeLetra", "Son [" + letrasParteEntera + " Pesos " + intDecParts[1] + "/100 M.N.]");
		parameters.put("LeyendaFotter", "Este no es un comprobante fiscal");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		InputStream inputStream = null;
		
		JasperDesign jasperDesign = null;
		long t1,t2,t3,t4,t5;
		t1=System.currentTimeMillis();
		try {
			logger.debug("\t=>JasperDesign before loar jrxml");			
			inputStream = TicketPOSTermalPrinter.class.getResourceAsStream(reportPath);
			logger.debug("\t=>stream loaded ="+inputStream);	
			jasperDesign = JRXmlLoader.load(inputStream);
		} catch (JRException ex) {
			throw new IOException("No s epuede cargar el Jasper Report:" + reportPath+":"+ex.getMessage());
		}
		int pageHeight = jasperDesign.getPageHeight(); //jasperReport.getPageHeight();
		int titleHeight = jasperDesign.getTitle().getHeight();
		int pageHeaderHeight = jasperDesign.getPageHeader().getHeight();
		int columnHeaderHeight = jasperDesign.getColumnHeader().getHeight();
		if (parameters.get("rfc") == null) {
			pageHeaderHeight = 0;
		}
		int detailHeight = jasperDesign.getDetailSection().getBands()[0].getHeight();
		int sumaryHeight = jasperDesign.getSummary().getHeight();
		int allBandsHeight = titleHeight + pageHeaderHeight + columnHeaderHeight + detailHeight + sumaryHeight;
		int exactPageHeight = titleHeight + pageHeaderHeight + columnHeaderHeight + detailHeight * numReg + sumaryHeight;

		logger.debug("Ok, JasperDesign created: pageHeight=" + pageHeight + ", pageHeaderHeight=" + pageHeaderHeight + ", columnHeaderHeight=" + columnHeaderHeight + ", detailHeight=" + detailHeight + ", allBandsHeight=" + allBandsHeight + ", exactPageHeight=" + exactPageHeight);
		jasperDesign.setPageHeight(exactPageHeight);
		logger.debug("\t=>JasperDesign pageHeight=" + jasperDesign.getPageHeight());
		
		logger.debug("Ok, JRDataSource preprared, to Load Jasper : from"+compiledReportPath);
		JasperReport jasperReport = null;
		try {
			t2=System.currentTimeMillis();
			logger.debug("\t=>Before Load Compiled: DT="+(t2-t1));			
			jasperReport = JasperCompileManager.compileReport(jasperDesign);
			//inputStream = TicketPOSTermalPrinter.class.getResourceAsStream(compiledReportPath);
			//logger.debug("\t=>stream loaded ="+inputStream);	
			//jasperReport = (JasperReport) JRLoader.loadObject(inputStream);
			
			t3=System.currentTimeMillis();
			logger.debug("\t=>JasperDesign after load compiled: DT="+(t3-t2));
			
			logger.debug("\t==>Before fillReport");
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
			t4=System.currentTimeMillis();
			logger.debug("\t=>JasperPrint after fill: DT="+(t4-t3));			
			
			fileGenarated = "jasper_out_" + sdf.format(new Date()) + ".pdf";
			final String fileGenaratedPDF =  fileGenarated;
			final JasperPrint jasperPrintPDF = jasperPrint;
			new Thread(){
				@Override
				public void run() {
					try{
						JasperExportManager.exportReportToPdfFile(jasperPrintPDF, fileGenaratedPDF);
					}catch(Exception ex){
						logger.error("Generating internal PDF", ex);
					}
				}			
			}.start();			
			//---------------------------------------------------------
			//t5=System.currentTimeMillis();
			//logger.debug("\t=>JasperPrint after export: DT="+(t5-t4));			
			
			//logger.debug("Ok, JasperExportManager executed");
			//JasperPrintManager.printReport(jasperPrint, false);
			//logger.debug("Ok, printed. executed");
		} catch (JRException ex) {
			throw new IOException("Al generar el Ticket:" + ex);
		}
		return jasperPrint;

	}
	

	@Override
	public void sendToPrinter(Object objectToPrint) throws IOException {
		try {
			JasperPrint jasperPrint = (JasperPrint) objectToPrint;
			logger.debug("Ok, JasperExportManager executed");
			JasperPrintManager.printReport(jasperPrint, false);
			logger.debug("Ok, printed job sent");
		} catch (JRException ex) {
			throw new IOException("Al enviar el Ticket a la Impresora:" + ex.getMessage());
		}
	}

	@Override
	public void testDefaultPrinter() throws IOException {
		logger.debug("-->>>generateTicket: testDefaultPrinter");

		JasperPrint jasperPrint = null;
		String reportPath		= "/ticket_layout/TestPrint_SUC_2.jrxml";
		
		JRDataSource beanColDataSource = new JREmptyDataSource();
		
		Map parameters = new HashMap();

		SimpleDateFormat sdf_f1 = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdf_h1 = new SimpleDateFormat("HH:mm");
		SimpleDateFormat sdf    = new SimpleDateFormat("yyyyMMdd_HHmmss");
		
		Date fechaReporte = new Date();
		
		parameters.put("fecha", sdf_f1.format(fechaReporte));
		parameters.put("hora", sdf_h1.format(fechaReporte));
		
		JasperDesign jasperDesign = null;
		long t1,t2,t3,t4,t5;
		t1=System.currentTimeMillis();
		InputStream inputStream  =null;
		try {
			logger.debug("\t=>JasperDesign before loar jrxml");			
			inputStream = TicketPOSTermalPrinter.class.getResourceAsStream(reportPath);
			logger.debug("\t=>stream loaded ="+inputStream);	
			jasperDesign = JRXmlLoader.load(inputStream);
		} catch (JRException ex) {
			throw new IOException("No s epuede cargar el Jasper Report:" + reportPath+":"+ex.getMessage());
		}
		JasperReport jasperReport = null;
		try {
			t2=System.currentTimeMillis();
			logger.debug("\t=>Before Load Compiled: DT="+(t2-t1));			
			jasperReport = JasperCompileManager.compileReport(jasperDesign);
			//inputStream = TicketPOSTermalPrinter.class.getResourceAsStream(compiledReportPath);
			//logger.debug("\t=>stream loaded ="+inputStream);	
			//jasperReport = (JasperReport) JRLoader.loadObject(inputStream);
			
			t3=System.currentTimeMillis();
			logger.debug("\t=>JasperDesign after load compiled: DT="+(t3-t2));
			
			logger.debug("\t==>Before fillReport");
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
			t4=System.currentTimeMillis();
			logger.debug("\t=>JasperPrint after fill: DT="+(t4-t3));			
			String fileGenarated = "TestPrint_SUC_2_" + sdf.format(new Date()) + ".pdf";
			JasperExportManager.exportReportToPdfFile(jasperPrint, fileGenarated);
			//---------------------------------------------------------
			JasperPrintManager.printReport(jasperPrint, false);
			logger.debug("Ok, printed. executed");
		} catch (JRException ex) {
			throw new IOException("Al generar el Ticket:" + ex);
		}
		
	}
	
	@Override
	public void setApplicationLogic(ApplicationLogic al) {
		applicationLogic = al;
	}
}
