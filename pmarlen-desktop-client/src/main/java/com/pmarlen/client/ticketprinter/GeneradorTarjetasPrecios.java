/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.client.ticketprinter;

import com.pmarlen.model.beans.Almacen;
import com.pmarlen.model.beans.AlmacenProducto;
import com.pmarlen.model.beans.Producto;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author alfredo
 */
public class GeneradorTarjetasPrecios {

	public static void generateTicket(List<Producto> listaDeProductos,Almacen almacen) throws IOException {
		JasperPrint jasperPrint = null;
		String reportPath       = "/ticket_layout/TARJETAS_DE_PRECIOS.jrxml";
		int contPrecCero=0;
		Collection<Map<String, ?>> col = new ArrayList<Map<String, ?>>();
		DecimalFormat df = new DecimalFormat("$###,###,###,##0.00");
		final Iterator<Producto> iterator = listaDeProductos.iterator();
		while(iterator.hasNext()){
		
			Map<String, String> vals = new HashMap<String, String>();
			final int maxColumns = 6;
			
			for(int numcol=0; numcol<maxColumns ;numcol++){
				vals.put("nombre_"+numcol		, "");
				vals.put("presentacion_"+numcol	, "");
				vals.put("codigobarras_"+numcol	, "");
				vals.put("precio_"+numcol		, "");
			}
			for(int numcol=0; iterator.hasNext() && numcol<maxColumns ;numcol++){
				final Producto producto = iterator.next();
				double precioVenta = 0.0;
				final Collection<AlmacenProducto> almacenProductoCollection = producto.getAlmacenProductoCollection();
				for(AlmacenProducto ap: almacenProductoCollection){
					if(ap.getAlmacen().getId().intValue() == almacen.getId().intValue()){
						precioVenta = ap.getPrecioVenta();
						if(precioVenta == 0.0){
							contPrecCero ++;
						}
					}
				}
				
				if(producto!=null && precioVenta>0){
					vals.put("nombre_"+numcol		, producto.getNombre());
					vals.put("presentacion_"+numcol	, producto.getPresentacion()+" "+producto.getContenido()+" "+producto.getUnidadMedida());
					vals.put("codigobarras_"+numcol	, "[ "+producto.getCodigoBarras()+" ]");
					vals.put("precio_"+numcol		, df.format(precioVenta));
				} else {
					vals.put("nombre_"+numcol		, "");
					vals.put("presentacion_"+numcol	, "");
					vals.put("codigobarras_"+numcol	, "");
					vals.put("precio_"+numcol		, "");
				}
				
			}
			col.add(vals);
		}
		
		System.err.println("-->> contPrecCero="+contPrecCero);
			
		JRDataSource beanColDataSource = new JRMapCollectionDataSource(col);
		System.err.println("Ok, JRDataSource created");

		Map parameters = new HashMap();
		InputStream inputStream = null;
		JasperDesign jasperDesign = null;
		JasperReport jasperReport = null;
		try {
			inputStream = GeneradorTarjetasPrecios.class.getResourceAsStream(reportPath);
			jasperDesign = JRXmlLoader.load(inputStream);		
			jasperReport = JasperCompileManager.compileReport(jasperDesign);
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
			
			JasperExportManager.exportReportToPdfFile(jasperPrint, "tarjetas_precios.pdf");			
		} catch (JRException ex) {
			throw new IOException("Al generar el Ticket:" + ex);
		}
		
	}
	
	public static void generateTicket(Producto productoImprimir,Almacen almacen) throws IOException {
		JasperPrint jasperPrint = null;
		String reportPath       = "/ticket_layout/TARJETAS_DE_PRECIOS_EPSON.jrxml";
		int contPrecCero=0;
		Collection<Map<String, ?>> col = new ArrayList<Map<String, ?>>();
		DecimalFormat df = new DecimalFormat("$###,###,###,##0.00");
		ArrayList<Producto> listaProductos = new ArrayList<Producto>();
		listaProductos.add(productoImprimir);
		final Iterator<Producto> iterator = listaProductos.iterator();
		while(iterator.hasNext()){
		
			Map<String, String> vals = new HashMap<String, String>();
			final int maxColumns = 1;
			
			for(int numcol=0; numcol<maxColumns ;numcol++){
				vals.put("nombre_"+numcol		, "");
				vals.put("presentacion_"+numcol	, "");
				vals.put("codigobarras_"+numcol	, "");
				vals.put("precio_"+numcol		, "");
			}
			for(int numcol=0; iterator.hasNext() && numcol<maxColumns ;numcol++){
				final Producto producto = iterator.next();
				double precioVenta = 0.0;
				final Collection<AlmacenProducto> almacenProductoCollection = producto.getAlmacenProductoCollection();
				for(AlmacenProducto ap: almacenProductoCollection){
					if(ap.getAlmacen().getId().intValue() == almacen.getId().intValue()){
						precioVenta = ap.getPrecioVenta();
						if(precioVenta == 0.0){
							contPrecCero ++;
						}
					}
				}
				
				if(producto!=null && precioVenta>0){
					vals.put("nombre_"+numcol		, producto.getNombre());
					vals.put("presentacion_"+numcol	, producto.getPresentacion()+" "+producto.getContenido()+" "+producto.getUnidadMedida());
					vals.put("codigobarras_"+numcol	, "[ "+producto.getCodigoBarras()+" ]");
					vals.put("precio_"+numcol		, df.format(precioVenta));
				} else {
					vals.put("nombre_"+numcol		, "");
					vals.put("presentacion_"+numcol	, "");
					vals.put("codigobarras_"+numcol	, "");
					vals.put("precio_"+numcol		, "");
				}
				
			}
			col.add(vals);
		}
		
		System.err.println("-->> contPrecCero="+contPrecCero);
			
		JRDataSource beanColDataSource = new JRMapCollectionDataSource(col);
		System.err.println("Ok, JRDataSource created");

		Map parameters = new HashMap();
		InputStream inputStream = null;
		JasperDesign jasperDesign = null;
		JasperReport jasperReport = null;
		try {
			inputStream = GeneradorTarjetasPrecios.class.getResourceAsStream(reportPath);
			jasperDesign = JRXmlLoader.load(inputStream);		
			jasperReport = JasperCompileManager.compileReport(jasperDesign);
			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
			
			JasperExportManager.exportReportToPdfFile(jasperPrint, "tarjetas_precios.pdf");			
			JasperPrintManager.printReport(jasperPrint, false);
		} catch (JRException ex) {
			throw new IOException("Al generar el Ticket:" + ex);
		}
		
	}
}
