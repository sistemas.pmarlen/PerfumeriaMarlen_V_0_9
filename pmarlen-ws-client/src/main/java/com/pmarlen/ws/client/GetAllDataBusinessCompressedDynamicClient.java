package com.pmarlen.ws.client;

import com.pmarlen.model.beans.*;
import com.pmarlen.model.beans.Usuario;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.pmarlen.wscommons.services.GetListDataBusiness;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetAllDataBusinessCompressedDynamicClient {

    private static Logger logger = LoggerFactory.getLogger(GetAllDataBusinessCompressedDynamicClient.class);

    private GetAllDataBusinessCompressedDynamicClient () {
    }

    public static void main(String args[]) throws Exception {
        // START SNIPPET: client
        //logger.info("==>> 1: init contex");
        //ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"client-beans.xml"});
        //logger.info("==>> 2: contex ok");
		
        GetListDataBusiness client = null;
        JaxWsProxyFactoryBean factory = null;
        try {
			
			String server = args[0];
			String port   = args[1];
			
            logger.info("==>> 3:Creating Factory");
            factory = new JaxWsProxyFactoryBean();
            
            factory.setServiceClass(GetListDataBusiness.class);
            logger.info("==>> 3.5:factory.adress ="+factory.getAddress());
            
            factory.setAddress("http://"+server+":"+port+"/pmarlen-webrf3/services/GetListDataBusiness");
            
            logger.info("==>> 4:Creating Client");

            Object obj = factory.create();

            logger.info("==>> 4.1:obj.class="+obj.getClass());
            logger.info("==>> 4.2:obj.super class="+obj.getClass().getSuperclass());
            logger.info("==>> 4.3:obj.interfaces="+Arrays.asList(obj.getClass().getInterfaces()));

            client = (GetListDataBusiness)obj;
            logger.info("==>> 5:Invoking services");
			long t0= System.currentTimeMillis();
            logger.info("==>> 5.1:Invoking service getAllDataPackCompressed()");

			byte[] bytes = client.getAllDataPackCompressed();
			long t1= System.currentTimeMillis();
			logger.info("==>> 5.2: T="+(t1-t0)+" millisecs, bytes length="+bytes.length);

			ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
			GZIPInputStream gzipIn = new GZIPInputStream(bais);
			ObjectInputStream objectIn = new ObjectInputStream(gzipIn);
			
			List allObjects=(List)objectIn.readObject();
			int listIndex=0;
			
            List<Usuario> listaUsuarios = (List<Usuario>)allObjects.get(listIndex++);

            for(Usuario x: listaUsuarios) {
                //logger.info("\t-> Usuario:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			long t2= System.currentTimeMillis();
			logger.info("==>> 5.3: T="+(t2-t1)+" millisecs, listaUsuarios.size="+listaUsuarios.size());
			
            List<Industria> listaIndustria = (List<Industria>)allObjects.get(listIndex++);

            for(Industria x: listaIndustria) {
                //logger.info("\t-> Industria:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			long t3= System.currentTimeMillis();
			logger.info("==>> 5.4: T="+(t3-t2)+" millisecs, listaIndustria.size="+listaIndustria.size());
			

            List<Linea> listaLinea = (List<Linea>)allObjects.get(listIndex++);

            for(Linea x: listaLinea) {
                //logger.info("\t-> Linea:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			long t4= System.currentTimeMillis();
			logger.info("==>> 5.5: T="+(t4-t3)+" millisecs, listaLinea.size="+listaLinea.size());

            List<Marca> listaMarca = (List<Marca>)allObjects.get(listIndex++);

            for(Marca x: listaMarca) {
                //logger.info("\t-> Marca:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			long t5= System.currentTimeMillis();
			logger.info("==>> 5.6: T="+(t5-t4)+" millisecs, listaMarca.size="+listaMarca.size());

            List<Producto> listaProducto = (List<Producto>)allObjects.get(listIndex++);

            for(Producto x: listaProducto) {
                //logger.info("\t-> Producto:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			long t6= System.currentTimeMillis();
			logger.info("==>> 5.7: T="+(t6-t5)+" millisecs, listaProducto.size="+listaProducto.size());
			
            List<Multimedio> listaMultimedio = (List<Multimedio>)allObjects.get(listIndex++);
            if(listaMultimedio != null) {
				for(Multimedio x: listaMultimedio) {
					//logger.info("\t-> Multimedio:RutaContenido="+x.getRutaContenido()+", NombreArchivo=" + x.getNombreArchivo()+", MimeType="+x.getMimeType()+", Size="+x.getSizeBytes());                    
				}
			}
			long t7= System.currentTimeMillis();
			logger.info("==>> 5.8: T="+(t7-t6)+" millisecs, listaMultimedio.size="+listaMultimedio.size());
			
			List<AlmacenProducto> listaAlmacenProducto = (List<AlmacenProducto>)allObjects.get(listIndex++);
            if(listaAlmacenProducto != null) {
				for(AlmacenProducto x: listaAlmacenProducto) {
					//logger.info("\t-> AlmacenProducto:"+ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
				}
			}
			long t8= System.currentTimeMillis();
			logger.info("==>> 5.9: T="+(t8-t7)+" millisecs, listaAlmacenProducto.size="+listaAlmacenProducto.size());

            List<FormaDePago> listaFormaDePago = (List<FormaDePago>)allObjects.get(listIndex++);
            if(listaFormaDePago != null){
                for(FormaDePago x: listaFormaDePago) {
                    //logger.info("\t-> FormaDePago:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
                }
            }
			long t9= System.currentTimeMillis();
			logger.info("==>> 5.10: T="+(t9-t8)+" millisecs, listaFormaDePago.size="+listaFormaDePago.size());

            List<Estado> listaEstado = (List<Estado>)allObjects.get(listIndex++);
            if(listaEstado != null){
                for(Estado x: listaEstado) {
                    //logger.info("\t-> Estado:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
                }
            }
			long t10= System.currentTimeMillis();
			logger.info("==>> 5.11: T="+(t10-t9)+" millisecs, listaEstado.size="+listaEstado.size());

			List<Sucursal> listaSucursal = (List<Sucursal>)allObjects.get(listIndex++);
			
            if(listaSucursal != null){
                for(Sucursal x: listaSucursal) {
                    //logger.info("\t-> Sucursal:" + x.getId());
					Collection<Usuario> usuarioCollection = x.getUsuarioCollection();
					
					if(usuarioCollection != null) {
						for(Usuario u: usuarioCollection){
							//logger.info("\t\t-> Usuario:" + u.getUsuarioId()); 
						}
					}
					
					Collection<Almacen> almacenCollection = x.getAlmacenCollection();
					if(almacenCollection != null) {
						for(Almacen almacen: almacenCollection){
							//logger.info("\t\t-> Almacen:" + almacen.getId());
							Collection<AlmacenProducto> almacenProductoCollection = almacen.getAlmacenProductoCollection();
							if(almacenProductoCollection != null) {
								for(AlmacenProducto ap: almacenProductoCollection){
									//logger.info("\t\t\t-> Producto:"+ap.getProducto().getId()+" x "+ap.getCantidadActual()); 
								}
							}
						}
					}
                }
            }
			long t11= System.currentTimeMillis();
			logger.info("==>> 5.12: T="+(t11-t10)+" millisecs, listaSucursal.size="+listaSucursal.size());

            List<Cliente> listaCliente = (List<Cliente>)allObjects.get(listIndex++);
            if(listaCliente != null){
                for(Cliente x: listaCliente) {
                    //logger.info("\t-> Cliente:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
                }
            }
			long t12= System.currentTimeMillis();
			logger.info("==>> 5.13: T="+(t12-t11)+" millisecs, listaCliente.size="+listaCliente.size());
			
			logger.info("\t==>> 6: T="+(t1-t0)+" + "+(t12-t1)+" = "+(t12-t0)+" millisecs, all process");

        } catch(Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }        
    }
}
