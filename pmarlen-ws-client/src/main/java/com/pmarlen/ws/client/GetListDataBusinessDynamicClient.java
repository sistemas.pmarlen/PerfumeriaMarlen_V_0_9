package com.pmarlen.ws.client;

import com.pmarlen.model.beans.*;
import com.pmarlen.model.beans.Usuario;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.pmarlen.wscommons.services.GetListDataBusiness;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetListDataBusinessDynamicClient {

    private static Logger logger = LoggerFactory.getLogger(GetListDataBusinessDynamicClient.class);

    private GetListDataBusinessDynamicClient () {
    }

    public static void main(String args[]) throws Exception {
        // START SNIPPET: client
        //logger.info("==>> 1: init contex");
        //ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"client-beans.xml"});
        //logger.info("==>> 2: contex ok");
		
        GetListDataBusiness client = null;
        JaxWsProxyFactoryBean factory = null;
        try {
			
			String server = args[0];
			String port   = args[1];
			
            logger.info("==>> 3:Creating Factory");
            factory = new JaxWsProxyFactoryBean();
            
            factory.setServiceClass(GetListDataBusiness.class);
            logger.info("==>> 3.5:factory.adress ="+factory.getAddress());
            
            factory.setAddress("http://"+server+":"+port+"/pmarlen-webrf3/services/GetListDataBusiness");
            
            logger.info("==>> 4:Creating Client");

            Object obj = factory.create();

            logger.info("==>> 4.1:obj.class="+obj.getClass());
            logger.info("==>> 4.2:obj.super class="+obj.getClass().getSuperclass());
            logger.info("==>> 4.3:obj.interfaces="+Arrays.asList(obj.getClass().getInterfaces()));

            client = (GetListDataBusiness)obj;
            logger.info("==>> 5:Invoking services");
            logger.info("==>> 5.1:Invoking service getUsuarioList()");

            List<Usuario> listaUsuarios = client.getUsuarioList();

            for(Usuario x: listaUsuarios) {
                logger.info("\t-> Usuario:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			
            logger.info("==>> 5.2:Invoking service getIndustriaList()");

            List<Industria> listaIndustria = client.getIndustriaList();

            for(Industria x: listaIndustria) {
                logger.info("\t-> Industria:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }

            logger.info("==>> 5.3:Invoking service getLineaList()");

            List<Linea> listaLinea = client.getLineaList();

            for(Linea x: listaLinea) {
                logger.info("\t-> Linea:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }

            logger.info("==>> 5.4:Invoking service getMarcaList()");

            List<Marca> listaMarca = client.getMarcaList();

            for(Marca x: listaMarca) {
                logger.info("\t-> Marca:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }

            logger.info("==>> 5.5:Invoking service getProductoList()");

            List<Producto> listaProducto = client.getProductoList();

            for(Producto x: listaProducto) {
                logger.info("\t-> Producto:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
            }
			
            logger.info("==>> 5.7:Invoking service getProductoMultimedioList()");

            List<Multimedio> listaMultimedio = client.getProductoMultimedioList();
            logger.info("==>> 5.7.1:listaMultimedio is null? "+(listaMultimedio == null));
            if(listaMultimedio!= null){
                for(Multimedio x: listaMultimedio) {
                    logger.info("\t-> Multimedio:RutaContenido="+x.getRutaContenido()+", NombreArchivo=" + x.getNombreArchivo()+", MimeType="+x.getMimeType()+", Size="+x.getSizeBytes());                    
                }
            }

            logger.info("==>> 5.8:Invoking service getFormaDePagoList()");

            List<FormaDePago> listaFormaDePago = client.getFormaDePagoList();
            if(listaFormaDePago != null){
                for(FormaDePago x: listaFormaDePago) {
                    logger.info("\t-> FormaDePago:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
                }
            }

            logger.info("==>> 5.9:Invoking service getEstadoList()");

            List<Estado> listaEstado = client.getEstadoList();
            if(listaEstado != null){
                for(Estado x: listaEstado) {
                    logger.info("\t-> Estado:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
                }
            }

            logger.info("==>> 5.10:Invoking service getClienteList()");

            List<Cliente> listaCliente = client.getClienteList();
            if(listaCliente != null){
                for(Cliente x: listaCliente) {
                    logger.info("\t-> Cliente:" + ReflectionToStringBuilder.toString(x, ToStringStyle.SIMPLE_STYLE));
                }
            }

			logger.info("==>> 5.11:Invoking service getSucursalList()");

            List<Sucursal> listaSucursal = client.getSucursalList();
            if(listaSucursal != null){
                for(Sucursal x: listaSucursal) {
                    logger.info("\t-> Sucursal:" + x.getId());
					Collection<Usuario> usuarioCollection = x.getUsuarioCollection();
					
					if(usuarioCollection != null) {
						for(Usuario u: usuarioCollection){
							logger.info("\t\t-> Usuario:" + u.getUsuarioId()); 
						}
					}
					
					Collection<Almacen> almacenCollection = x.getAlmacenCollection();
					if(almacenCollection != null) {
						for(Almacen almacen: almacenCollection){
							logger.info("\t\t-> Almacen:" + almacen.getId());
							Collection<AlmacenProducto> almacenProductoCollection = almacen.getAlmacenProductoCollection();
							if(almacenProductoCollection != null) {
								for(AlmacenProducto ap: almacenProductoCollection){
									logger.info("\t\t\t-> Producto:"+ap.getProducto().getId()+" x "+ap.getCantidadActual()); 
								}
							}
						}
					}
                }
            }

            logger.info("==>> 5.12:Invoking service getServerVersion()");

            String serverVersion = client.getServerVersion();
            logger.info("\t-> serverVersion:"+serverVersion);

        } catch(Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }        
    }
}
