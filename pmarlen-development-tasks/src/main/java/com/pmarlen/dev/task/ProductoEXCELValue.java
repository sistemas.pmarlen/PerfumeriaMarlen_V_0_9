package com.pmarlen.dev.task;

public class ProductoEXCELValue {
	
    Integer proveedorId; 
	
    Integer id; 
    String  codigoBarras;
    String  descripcion;
    String  presentacion;
    Double  cont;
    String  unidadMedida;
    String  unidadesXCaja;
	
    Double  costo;
	Double  costoVenta;
	Double  costoVentaSc;
	Double  costoVentaOp;
	
    Double  precioVenta;
	Double  precioVentaOp;
    Double  precioVentaSuc;
	Double  precioVentaSucOp;
	Double  precioVentaReg;
	
	int     cantidadAlmacen;
	int     cantidadAlmacenOp;
	int     cantidadAlmacenReg;
	
	int     sucCanAlmacen;
	int     sucCanAlmacenOp;
	int     sucCanAlmacenReg;
	
    MarcaEXCELValue marca;
	ProveedorEXCELValue pev;
	String abrev;
	
}
