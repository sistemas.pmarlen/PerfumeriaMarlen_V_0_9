
package com.pmarlen.wscommons.services.dto;

import java.io.Serializable;
import java.util.Set;
import java.util.Collection;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Embeddable;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.EmbeddedId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.JoinTable;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Class for mapping DTO Entity of Table Entrada_Salida_Directa_Almacen.
 * 
 * @author Tracktopell::jpa-builder @see  https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @version 0.8.5
 * @date 2014/03/16 06:37
 */

public class EntradaSalidaDirectaAlmacen implements java.io.Serializable {
    private static final long serialVersionUID = 361854905;
    
    /**
    * id
    */
    private Integer id;
    
    /**
    * almacen id
    */
    private Almacen almacen;
    
    /**
    * usuario id
    */
    private Usuario usuario;
    
    /**
    * fecha
    */
    private java.util.Date fecha;
    
    /**
    * comentarios
    */
    private String comentarios;
    
    /**
    * tipo movimiento id
    */
    private TipoMovimiento tipoMovimiento;
    
    private Collection<EntradaSalidaDirectaAlmacenDetalle> entradaSalidaDirectaAlmacenDetalleCollection;
    

    /** 
     * Default Constructor
     */
    public EntradaSalidaDirectaAlmacen() {
    }

    /** 
     * lazy Constructor just with IDs
     */
    public EntradaSalidaDirectaAlmacen( Integer id ) {
        this.id 	= 	id;

    }
    
    /**
     * Getters and Setters
     */
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer v) {
        this.id = v;
    }

    public Almacen getAlmacen () {
        return this.almacen;
    }

    public void setAlmacen(Almacen v) {
        this.almacen = v;
    }

    public Usuario getUsuario () {
        return this.usuario;
    }

    public void setUsuario(Usuario v) {
        this.usuario = v;
    }

    public java.util.Date getFecha() {
        return this.fecha;
    }

    public void setFecha(java.util.Date v) {
        this.fecha = v;
    }

    public String getComentarios() {
        return this.comentarios;
    }

    public void setComentarios(String v) {
        this.comentarios = v;
    }

    public TipoMovimiento getTipoMovimiento () {
        return this.tipoMovimiento;
    }

    public void setTipoMovimiento(TipoMovimiento v) {
        this.tipoMovimiento = v;
    }

    
    public Collection<EntradaSalidaDirectaAlmacenDetalle> getEntradaSalidaDirectaAlmacenDetalleCollection() {
        return this.entradaSalidaDirectaAlmacenDetalleCollection;
    }
    
    
    public void setEntradaSalidaDirectaAlmacenDetalleCollection(Collection<EntradaSalidaDirectaAlmacenDetalle>  v) {
        this.entradaSalidaDirectaAlmacenDetalleCollection = v;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash = (id != null ? id.hashCode() : 0 );
        return hash;
    }

    public boolean equals(Object o){

        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(o instanceof EntradaSalidaDirectaAlmacen)) {
            return false;
        }

    	EntradaSalidaDirectaAlmacen other = (EntradaSalidaDirectaAlmacen ) o;
        if ( (this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }


    	return true;
    }

    @Override
    public String toString() {
        return "com.pmarlen.wscommons.services.dto.EntradaSalidaDirectaAlmacen[id = "+id+ "]";
    }

}
