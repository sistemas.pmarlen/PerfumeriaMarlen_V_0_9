package com.pmarlen.businesslogic;

import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.*;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import java.util.Collection;
import java.util.Date;

import java.util.ArrayList;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * PedidoVentaBusinessLogic
 */

@Repository("entradaSalidaDirectaBusinessLogic")
public class EntradaSalidaDirectaBusinessLogic {
	
	private Logger logger;

	private EntityManagerFactory emf = null;

    @Autowired
    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }
	
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
	
	public EntradaSalidaDirectaBusinessLogic() {
		logger = LoggerFactory.getLogger(EntradaSalidaDirectaBusinessLogic.class);        
        logger.debug("->EntradaSalidaDirectaBusinessLogic, created");
	}
	
	public EntradaSalidaDirectaAlmacen crearESDA(EntradaSalidaDirectaAlmacen esdaX){
		logger.debug("-->>crearESDA: esdaX.getTipoMovimiento()="+esdaX.getTipoMovimiento().getId()+", almacenId:"+esdaX.getAlmacen().getId());
		int afectacion =0;
			
		if (esdaX.getTipoMovimiento().getId().intValue()>=Constants.TIPO_MOV_SALIDA_ALMACEN && esdaX.getTipoMovimiento().getId().intValue()<=Constants.TIPO_MOV_SALIDA_DEV ){
			afectacion = -1;
		} else if (esdaX.getTipoMovimiento().getId().intValue()>=Constants.TIPO_MOV_ENTRADA_ALMACEN && esdaX.getTipoMovimiento().getId().intValue()<=Constants.TIPO_MOV_ENTRADA_ALMACEN_DEV ){
			afectacion =  1;
		} else {
			throw new IllegalStateException("Tipo de Mmovimeito Ilegal");
		}

        EntityManager em = null;
		EntradaSalidaDirectaAlmacen esda=null;
		Collection<EntradaSalidaDirectaAlmacenDetalle> esdadListX = esdaX.getEntradaSalidaDirectaAlmacenDetalleCollection();
		Collection<EntradaSalidaDirectaAlmacenDetalle> esdadList = null;
		try {
            em = getEntityManager();
            em.getTransaction().begin();
			
			esda = new EntradaSalidaDirectaAlmacen();
			
			esda.setAlmacen(esdaX.getAlmacen());
			esda.setComentarios(esdaX.getComentarios());
			esda.setFecha(esdaX.getFecha());
			esda.setTipoMovimiento(esdaX.getTipoMovimiento());
			esda.setUsuario(esdaX.getUsuario());
			
			em.persist(esda);
			esdadList = new ArrayList<EntradaSalidaDirectaAlmacenDetalle>();
			logger.debug("-->>crearESDA: \tAFECTACIÓN INVENTARIO: "+afectacion);

			for(EntradaSalidaDirectaAlmacenDetalle esdadX: esdadListX){
				EntradaSalidaDirectaAlmacenDetalle esdad=new EntradaSalidaDirectaAlmacenDetalle();
				
				esdad.setCantidad					(esdadX.getCantidad());
				esdad.setCosto						(esdadX.getCosto());
				esdad.setEntradaSalidaDirectaAlmacen(esda);
				esdad.setPrecioVenta				(esdadX.getPrecioVenta());
				esdad.setProducto					(esdadX.getProducto());
				
				em.persist(esdad);
				
				AlmacenProducto almacenProducto   = null;
				Query q = em.createQuery("select ap from AlmacenProducto ap where ap.almacen.id=:almacenId and ap.producto.id=:productoId");
				q.setParameter("almacenId" , esda.getAlmacen().getId());
				q.setParameter("productoId", esdad.getProducto().getId());
				
				almacenProducto   = (AlmacenProducto)q.getSingleResult();

                if(almacenProducto == null ) {
                    throw new IllegalStateException("No hay registro de almacen para el producto:"+esdadX.getProducto().getId());
                }
                
                int cantEnAlmacenTotal = almacenProducto.getCantidadActual() ;
                logger.debug("-->>crearESDA:\tENTRADA/SALIDA INVENTARIO: almacen:"+almacenProducto.getAlmacen().getId()+", producto="+
						esdad.getProducto().getCodigoBarras()+", cantidad="+esdad.getCantidad()+", precioVenta="+esdad.getPrecioVenta());
                
				if ((	esda.getTipoMovimiento().getId().intValue()>=Constants.TIPO_MOV_SALIDA_ALMACEN && 
						esda.getTipoMovimiento().getId().intValue()<=Constants.TIPO_MOV_SALIDA_DEV       ) &&
						esdad.getCantidad() > almacenProducto.getCantidadActual()){
					throw new IllegalStateException("No hay suficiente para modificar:ProductoId="+esdad.getProducto().getId()+", cantidad="+
							esdad.getCantidad()+", cantidadAlmacen=" +almacenProducto.getCantidadActual());
				}
				
				almacenProducto.setCantidadActual(almacenProducto.getCantidadActual() + afectacion*esdad.getCantidad());
				almacenProducto.setPrecioVenta   (esdad.getPrecioVenta());
				
				em.flush();
				//------------------------------------------------------
				MovimientoHistoricoProducto mhpFis = new MovimientoHistoricoProducto();

				mhpFis.setAlmacen(almacenProducto.getAlmacen());					
				mhpFis.setCantidad(esdad.getCantidad());
				mhpFis.setCosto(esdad.getProducto().getCosto());
				mhpFis.setProducto(esdad.getProducto());
				mhpFis.setTipoMovimiento(esda.getTipoMovimiento());
				mhpFis.setUsuario(esda.getUsuario());
				mhpFis.setAlmacen(almacenProducto.getAlmacen());
				mhpFis.setFecha(esda.getFecha());
				mhpFis.setProducto(esdad.getProducto());

				em.persist(mhpFis);
				
				em.flush();
				logger.debug("-->>crearESDA: \tOK detalle");
			}
			
			em.getTransaction().commit();
			logger.debug("-->>crearESDA: OK Commit");
			
		} catch(Exception e){
			logger.error("-->>crearESDA: "+e);			
			logger.debug("-->>crearESDA: rollback");
			em.getTransaction().rollback();			
		} finally {
            if (em != null) {
                em.close();
            }
        }
		return esda;
	}
	
    public PedidoVenta crearPedidoCapturado(PedidoVenta pedidoVentaX,Usuario usuarioModifico) {
        Collection<PedidoVentaDetalle> detalleVentaPedidoOrigList = pedidoVentaX.getPedidoVentaDetalleCollection();
		Collection<PedidoVentaDetalle> detalleVentaPedidoInsert   = new ArrayList<PedidoVentaDetalle>();
        logger.debug("->crearPedidoCapturado");
        EntityManager em = null;
		PedidoVenta pedidoVenta = null;
		try {
            em = getEntityManager();
            em.getTransaction().begin();
        
			pedidoVenta = new PedidoVenta();
		
			pedidoVenta.setComentarios(pedidoVentaX.getComentarios());
			pedidoVenta.setPorcentajeDescuentoCalculado(pedidoVentaX.getPorcentajeDescuentoCalculado());
			pedidoVenta.setPorcentajeDescuentoExtra    (pedidoVentaX.getPorcentajeDescuentoExtra());
			pedidoVenta.setDescuentoAplicado(pedidoVentaX.getDescuentoAplicado());
			pedidoVenta.setFactoriva(pedidoVentaX.getFactoriva());
			pedidoVenta.setCliente(pedidoVentaX.getCliente());
			pedidoVenta.setFormaDePago(pedidoVentaX.getFormaDePago());
			pedidoVenta.setMetodoDePago(pedidoVentaX.getMetodoDePago());
			
			pedidoVenta.setUsuario(pedidoVentaX.getUsuario());
			pedidoVenta.setAlmacen(pedidoVentaX.getAlmacen());
			
			logger.debug("-> antes pedidoVenta="+pedidoVenta+", detail size:"+detalleVentaPedidoOrigList.size()+", {"+pedidoVenta.getCliente()+", "+pedidoVenta.getComentarios()+", "+pedidoVenta.getDescuentoAplicado()+", "+pedidoVenta.getFactoriva()+", "+pedidoVenta.getFormaDePago()+", "+pedidoVenta.getUsuario()+"}");
			em.persist(pedidoVenta);
			//em.flush();
			//em.merge(pedidoVenta);			
			
			logger.debug("-->> despues pedidoVenta="+pedidoVenta+": {"+pedidoVenta.getCliente()+", "+pedidoVenta.getComentarios()+", "+pedidoVenta.getDescuentoAplicado()+", "+pedidoVenta.getFactoriva()+", "+pedidoVenta.getFormaDePago()+", "+pedidoVenta.getUsuario()+"}");
			
            FormaDePago formaDePago = pedidoVentaX.getFormaDePago();
            if (formaDePago != null) {
                formaDePago = em.getReference(FormaDePago.class, formaDePago.getId());
                pedidoVenta.setFormaDePago(formaDePago);
            }
            Cliente cliente = pedidoVentaX.getCliente();
            if (cliente != null) {
                cliente = em.getReference(Cliente.class, cliente.getId());
                
                pedidoVenta.setCliente(cliente);
            }
            Usuario usuario = pedidoVentaX.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(Usuario.class, usuario.getUsuarioId());
                pedidoVenta.setUsuario(usuario);
            }
						
			
            //em.persist(pedidoVenta);
			//em.flush();
			//em.merge(pedidoVenta);
			
			//logger.debug("==>>pedidoVenta, despues 2do persist:"+pedidoVenta);
			//em.getTransaction().commit();            
			//logger.debug("================================================>>T1.commit();");
			//em.getTransaction().begin();
        
            if(detalleVentaPedidoOrigList != null ){
                for(PedidoVentaDetalle dvp: detalleVentaPedidoOrigList ) {
					
					PedidoVentaDetalle detalleVentaPedido = new PedidoVentaDetalle();
					
					detalleVentaPedido.setCantidad(dvp.getCantidad());
					detalleVentaPedido.setPedidoVenta(pedidoVenta);
					detalleVentaPedido.setPrecioVenta(dvp.getPrecioVenta());
					detalleVentaPedido.setProducto(dvp.getProducto());
					
					detalleVentaPedidoInsert.add(detalleVentaPedido);
                    em.persist(detalleVentaPedido);  				
					
					//em.merge(detalleVentaPedido);
					//em.flush();
					logger.debug("\t->crearPedidoCapturado: detalleVentaPedido["+dvp.getCantidad()+" x "+dvp.getProducto()+"], pedidoVenta="+pedidoVenta+": persisted->>detalleVentaPedido="+detalleVentaPedido);
                }
            }
            pedidoVenta.setPedidoVentaDetalleCollection(detalleVentaPedidoInsert);
            
            
            PedidoVentaEstado pedidoVentaEstado = new PedidoVentaEstado();
            pedidoVentaEstado.setPedidoVenta(pedidoVenta);
            pedidoVentaEstado.setEstado(new Estado(Constants.ESTADO_CAPTURADO));
            
            Usuario usuarioModificoRefreshed = em.getReference(Usuario.class, usuarioModifico.getUsuarioId());
            pedidoVentaEstado.setEstado(new Estado(Constants.ESTADO_CAPTURADO));
            final Date dateCaptura = new Date();
            logger.debug("==>>crearPedidoCapturado():"+dateCaptura);
            pedidoVentaEstado.setFecha(dateCaptura);
            pedidoVentaEstado.setPedidoVenta(pedidoVenta);
            pedidoVentaEstado.setUsuario(usuarioModificoRefreshed);
            em.persist(pedidoVentaEstado);
            //em.flush();
			//em.merge(pedidoVenta);
			
            em.getTransaction().commit();            
			logger.debug("==>>commit final!");
			
			return pedidoVenta;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
	    
}
