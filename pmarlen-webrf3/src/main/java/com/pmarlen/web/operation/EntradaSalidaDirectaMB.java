/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pmarlen.web.operation;

import com.pmarlen.businesslogic.EntradaSalidaDirectaBusinessLogic;
import com.pmarlen.model.Constants;
import com.pmarlen.model.beans.Almacen;
import com.pmarlen.model.beans.AlmacenProducto;
import com.pmarlen.model.beans.EntradaSalidaDirectaAlmacen;
import com.pmarlen.model.beans.EntradaSalidaDirectaAlmacenDetalle;
import com.pmarlen.model.beans.Producto;
import com.pmarlen.model.beans.Sucursal;
import com.pmarlen.model.beans.TipoMovimiento;
import com.pmarlen.model.controller.ProductoJPAController;
import com.pmarlen.model.controller.SucursalJPAController;
import com.pmarlen.web.common.view.messages.Messages;
import com.pmarlen.web.security.managedbean.SessionUserMB;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author VEAXX9M
 */
public class EntradaSalidaDirectaMB {
    
    private ProductoJPAController productoJPAController;
    
    private SucursalJPAController sucursalJPAController;
    	    
    private SessionUserMB sessionUserMB;
	
	private EntradaSalidaDirectaBusinessLogic entradaSalidaDirectaBusinessLogic;
    
    private List<EntradaSalidaDirectaAlmacenDetalle> entradaSalidaDirectaAlmacenDetalleList;
	private EntradaSalidaDirectaAlmacen entradaSalidaDirectaAlmacen;
    private EntradaSalidaDirectaAlmacenDetalle entradaSalidaDirectaAlmacenDetalleSeleccionado;
    private String nombreDescripcion;
	private String codigoBuscar;
	private Producto productoEncontrado;
    private Integer productoSelected;
    private List<SelectItem> productoConNombreDescripcion;
    private Integer clienteId;
    private Integer formaDePagoId;
    private final Logger logger = LoggerFactory.getLogger(EntradaSalidaDirectaMB.class);
	private Almacen almacenObjetivo;
	private int modoVenta;
	private TipoMovimiento tipoMovimiento;
	
	private int descuentoCalculado;
	
	private int descuentoEspecial;
	
	private List<SelectItem> descuentosPosiblesList;

	private List<AlmacenProducto> listAlmacenProductoBuscar;
	
	private Integer cantidadAgregar;
	
	/**
	 * @return the descuentoCalculado
	 */
	public int getDescuentoCalculado() {
		return descuentoCalculado;
	}

	private Hashtable<Integer,String> tipoAlmacenHashTable;	
	private List<SelectItem> resultTipoAlmacenList;

	public Hashtable<Integer, String> getTipoAlmacenHashTable() {
		if(tipoAlmacenHashTable == null){
			tipoAlmacenHashTable = new Hashtable<Integer, String> ();
			
			tipoAlmacenHashTable.put(Constants.ALMACEN_LINEA		, Messages.getLocalizedString("COMMON_ALMACEN_LINEA"));
			tipoAlmacenHashTable.put(Constants.ALMACEN_OPORTUNIDAD	, Messages.getLocalizedString("COMMON_ALMACEN_OPORTUNIDAD"));
			tipoAlmacenHashTable.put(Constants.ALMACEN_REGALIAS		, Messages.getLocalizedString("COMMON_ALMACEN_REGALIAS"));			
		}
		return tipoAlmacenHashTable;
	}
	
	public List<SelectItem> getTipoAlmacenList() {
		if(resultTipoAlmacenList == null){
			
			resultTipoAlmacenList = new ArrayList<SelectItem>();
	
			resultTipoAlmacenList.add(new SelectItem(Constants.ALMACEN_LINEA		, getTipoAlmacenHashTable().get(Constants.ALMACEN_LINEA)));
			resultTipoAlmacenList.add(new SelectItem(Constants.ALMACEN_OPORTUNIDAD	, getTipoAlmacenHashTable().get(Constants.ALMACEN_OPORTUNIDAD)));
			resultTipoAlmacenList.add(new SelectItem(Constants.ALMACEN_REGALIAS		, getTipoAlmacenHashTable().get(Constants.ALMACEN_REGALIAS)));
		}
	    
        return resultTipoAlmacenList;
    }
	
	public String getTipoAlmacenSeleccionado() {
		if(modoVenta > 0){
			return getTipoAlmacenHashTable().get(modoVenta);
		} else{
			return "-";
		}
    }

	static Sucursal sucursalPrincipal;
	
	public void almacenSelected(ValueChangeEvent event){
		logger.info("## >> * almacenSelected: old=" + event.getOldValue()+", new="+event.getNewValue());        
		modoVenta = (Integer)event.getNewValue();
		actualizarAlmacenObjetivoDesdeModoVenta();
		listAlmacenProductoBuscar = null;
	}
	
	public void descuentosPosiblesListChanged(ValueChangeEvent e) {
        Integer newValue = (Integer) e.getNewValue();
        logger.debug("## >> descuentosPosiblesListChanged: newValue=" + newValue);
		this.descuentoEspecial = newValue;
	}
	
    public EntradaSalidaDirectaMB() {		
        reiniciarEstadoInicial();		
    }

    private void reiniciarEstadoInicial() {
        entradaSalidaDirectaAlmacenDetalleSeleccionado = new EntradaSalidaDirectaAlmacenDetalle();
        entradaSalidaDirectaAlmacenDetalleList = new ArrayList<EntradaSalidaDirectaAlmacenDetalle>();
        entradaSalidaDirectaAlmacen = new EntradaSalidaDirectaAlmacen();
		//entradaSalidaDirectaAlmacen.setUsuario(sessionUserMB.getUsuarioAuthenticated());
        productoConNombreDescripcion = new ArrayList<SelectItem>();
		descuentoCalculado=0;
		descuentoEspecial =0;
		modoVenta = 1;
		clienteId = null;
		formaDePagoId = null;
		descuentoEspecial = 0;
		almacenObjetivo = null;
		listAlmacenProductoBuscar = null;		
    }

    public void actualizarEstatusFiscal(ActionEvent e) {
        logger.debug("## >> actualizarEstatusFiscal: ");
    }
	
	public void modoVentaChanged(ValueChangeEvent e) {
        logger.debug("## >> modoVentaChanged: "+e.getOldValue()+" -->> "+e.getNewValue());		
    }
	
	public void modoVentaChangedAction(ActionEvent e) {
        logger.debug("## >> modoVentaChangedAction: "+modoVenta);		
    }
	
	public String modoVentaSaved() {
        logger.debug("## >> modoVentaSaved: modoVenta="+modoVenta);
		actualizarAlmacenObjetivoDesdeModoVenta();

		return "modoVentaSaved";
    }
		
	public void codigoBarrasChanged(ValueChangeEvent e) {
		String nuevoCodigoBuscar = e.getNewValue().toString();
        logger.debug("## >> codigoBarrasChanged: nuevoCodigoBuscar=" + nuevoCodigoBuscar+", search and add 1 !");
		try{
			productoEncontrado = productoJPAController.findEntityByReadableProperty(nuevoCodigoBuscar);
			agregarProductoADetalle(productoEncontrado.getId(),1);
			productoEncontrado = null;
			productoConNombreDescripcion = new ArrayList<SelectItem>();
			nombreDescripcion = null;
		}catch(NoResultException nre){
			logger.debug("## >> no necontrado:" + nuevoCodigoBuscar);
		} finally{
			codigoBuscar ="";
		}
    }
	
	public void agregar1ProductoXCodigoBarras(ActionEvent e) {
        logger.debug("## >> agregar1ProductoXCodigoBarras: codigoBuscar=" + codigoBuscar+" add 1 most easy !");
        
		try{
			productoEncontrado = productoJPAController.findEntityByReadableProperty(codigoBuscar);
			agregarProductoADetalle(productoEncontrado.getId(),1);
			productoEncontrado = null;
			productoConNombreDescripcion = new ArrayList<SelectItem>();
			nombreDescripcion = null;
		}catch(NoResultException nre){
			logger.debug("## >> no necontrado:" + codigoBuscar);
		} finally{
			codigoBuscar ="";
		}
    }
	
    public void agregarProductoBuscado(ActionEvent e) {
        logger.debug("## >> agregarProductoBuscado: productoSelected=" + productoSelected+", run just This !");
        agregarProductoADetalle(productoSelected,1);
        productoSelected = null;
        productoConNombreDescripcion = new ArrayList<SelectItem>();
        nombreDescripcion = null;
    }
	
	public void agregarNProductoBuscado(ActionEvent e) {
        logger.debug("## >> agregarNProductoBuscado: productoSelected=" + productoSelected+", cantidadAgregar="+cantidadAgregar);
        agregarProductoADetalle(productoSelected,cantidadAgregar);
        productoSelected = null;
        productoConNombreDescripcion = new ArrayList<SelectItem>();
        nombreDescripcion = null;
    }

    public void agregar1Producto(ActionEvent e) {
        FacesContext context = FacesContext.getCurrentInstance();
        Integer productoId = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("productoId"));

        logger.debug("## >> agregar1Producto: productoId=" + productoId);

        agregarProductoADetalle(productoId, cantidadAgregar);
    }
	
    private void agregarProductoADetalle(Integer productoIdAgregar, int cantidad) {
        EntradaSalidaDirectaAlmacenDetalle detalleVentaCompraAgregar = null;
        logger.debug("## >> agregarProductoADetalle: productoIdAgregar=" + productoIdAgregar);
		
        for (EntradaSalidaDirectaAlmacenDetalle dvp : entradaSalidaDirectaAlmacenDetalleList) {
            if (dvp.getProducto().getId().intValue() == productoIdAgregar.intValue()) {
				dvp.setCantidad(dvp.getCantidad() + cantidad);
                logger.debug("## >> agregarProductoADetalle: \tcodigoBarrasAgregar=" + dvp.getProducto().getCodigoBarras()+" += "+cantidad+" => "+dvp.getCantidad());
                return;
            }
        }
        try {
			detalleVentaCompraAgregar = new EntradaSalidaDirectaAlmacenDetalle();
			detalleVentaCompraAgregar.setCantidad(cantidad);

			Producto producto = productoJPAController.findById(productoIdAgregar);
			Collection<AlmacenProducto> almacenProductoCollection = producto.getAlmacenProductoCollection();
			int cantActualAlmacen = 0;
			double precioObjetivo = 0.0;
			for (AlmacenProducto almacenProducto : almacenProductoCollection) {
				if(almacenProducto.getAlmacen().getId().intValue() == getAlmacenObjetivo().getId().intValue()){
					precioObjetivo = almacenProducto.getPrecioVenta();					
					cantActualAlmacen = almacenProducto.getCantidadActual();
				}
			}

			if(listAlmacenProductoBuscar ==  null){
				getListAlmacenProductoBuscar();
			}

			detalleVentaCompraAgregar.setProducto(producto);                
			detalleVentaCompraAgregar.setPrecioVenta(precioObjetivo);

			entradaSalidaDirectaAlmacenDetalleList.add(detalleVentaCompraAgregar);				
			logger.debug("## >> agregarProductoADetalle: \t Ok, Add new");

        } catch (ValidatorException ve) {
            logger.debug("<<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    ve.getFacesMessage());
        }
    }
	
	public int getEntradaSalidaDirectaAlmacenDetalleListSize(){
		if(entradaSalidaDirectaAlmacenDetalleList==null){
			entradaSalidaDirectaAlmacenDetalleList = new ArrayList<EntradaSalidaDirectaAlmacenDetalle>();
		}
		return entradaSalidaDirectaAlmacenDetalleList.size();
	}

    public String confirmarEntradaSalida() {
        logger.debug("========================================================>>");
        logger.debug("-->>confirmarEntradaSalida()");
        logger.debug("========================================================>>");
        try {
            dataValidation();
            try {
				tipoMovimiento = new TipoMovimiento(Constants.TIPO_MOV_ENTRADA_ALMACEN_DEV);
				logger.debug("==>>> tipoMovimiento="+tipoMovimiento.getId());
				
                entradaSalidaDirectaAlmacen.setUsuario(sessionUserMB.getUsuarioAuthenticated());
                entradaSalidaDirectaAlmacen.setComentarios("Entrada Compra" + new Date());				
				entradaSalidaDirectaAlmacen.setAlmacen(almacenObjetivo);
				entradaSalidaDirectaAlmacen.setTipoMovimiento(tipoMovimiento);
				entradaSalidaDirectaAlmacen.setFecha(new Date());
                Collection<EntradaSalidaDirectaAlmacenDetalle> entradaSalidaDirectaAlmacenDetalleCollection = new ArrayList<EntradaSalidaDirectaAlmacenDetalle>();
				
                for(EntradaSalidaDirectaAlmacenDetalle pvdw:entradaSalidaDirectaAlmacenDetalleList){
                    entradaSalidaDirectaAlmacenDetalleCollection.add(pvdw);
                }
                for(EntradaSalidaDirectaAlmacenDetalle pvd: entradaSalidaDirectaAlmacenDetalleCollection){
                    logger.debug("\t==>>entradaSalidaDirectaAlmacenDetalleCollection:"+pvd.getCantidad()+" x "+pvd.getProducto());
                }
                entradaSalidaDirectaAlmacen.setEntradaSalidaDirectaAlmacenDetalleCollection(entradaSalidaDirectaAlmacenDetalleCollection);
				
				entradaSalidaDirectaAlmacen = entradaSalidaDirectaBusinessLogic.crearESDA(entradaSalidaDirectaAlmacen);
                logger.debug("<<===================== OK crearESDA =======================");
				reiniciarEstadoInicial();
				return "CompraCreado";
            } catch (Exception ex) {
                logger.debug("<<++++++++++++++++++++++++++++++++++++++++++++++++++");                
                logger.debug("Error in MB to create Compra:", ex);
                throw new ValidatorException(
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.toString(), ex.toString()));
            } finally {
                
            }
        } catch (ValidatorException ve) {
            logger.debug("<<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    ve.getFacesMessage());
            return null;
        }
    }
    
	public String cancelarCompra() {
        logger.debug("========================================================>>");
        logger.debug("-->>cancelarCompra():");
        logger.debug("========================================================>>");
		reiniciarEstadoInicial();
		return null;
	}
	
	public void cancelarCompra(ActionEvent e) {
		logger.debug("==========>>cancelarCompraVenta():");        
		reiniciarEstadoInicial();		
	}
    
	private void dataValidation() throws ValidatorException {
        logger.debug("\t## >> dataValidation: nothing to validate :) ");

//        if (formaDePagoId == null || (formaDePagoId != null && formaDePagoId.intValue() == 0)) {
//            logger.debug("\t\t## >> throw new ValidatorException FormaDePago!");
//            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de Validación:", "¡Debe seleccionalr la Forma de Pago !"));
//        }
    }

    public void seleccionarProducto(ActionEvent e) {
        FacesContext context = FacesContext.getCurrentInstance();
        Integer productoId = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("productoId"));

        logger.debug("## >> seleccionarProducto: productoId=" + productoId);
        boolean selectedFromDetalle = false;
        for (EntradaSalidaDirectaAlmacenDetalle dvp : entradaSalidaDirectaAlmacenDetalleList) {
            if (dvp.getProducto().getId() == productoId ) {
                entradaSalidaDirectaAlmacenDetalleSeleccionado.setProducto(dvp.getProducto());
                entradaSalidaDirectaAlmacenDetalleSeleccionado.setCantidad(dvp.getCantidad());
                selectedFromDetalle = true;
                break;
            }
        }
        if (!selectedFromDetalle) {
            logger.warn("\t## >> productoId=" + productoId + " => detalleVentaCompraSeleccionado is null");
        }
        logger.debug("## >> end: seleccionarProducto");
    }

    public void guardarCantidadEntradaSalidaDirectaAlmacenDetalleSeleccionado(ActionEvent e) {
        FacesContext context = FacesContext.getCurrentInstance();

        logger.debug("## >> guardarCantidadEntradaSalidaDirectaAlmacenDetalleSeleccionado: productoId=" + entradaSalidaDirectaAlmacenDetalleSeleccionado.getProducto().getId() + ", cantidad=" + entradaSalidaDirectaAlmacenDetalleSeleccionado.getCantidad());
        for (EntradaSalidaDirectaAlmacenDetalle dvp : entradaSalidaDirectaAlmacenDetalleList) {
            if (dvp.getProducto().getId() == entradaSalidaDirectaAlmacenDetalleSeleccionado.getProducto().getId()) {
                dvp.setCantidad(entradaSalidaDirectaAlmacenDetalleSeleccionado.getCantidad());
                logger.debug("\t## >> ok, edited ");
                break;
            }
        }
        entradaSalidaDirectaAlmacenDetalleSeleccionado = new EntradaSalidaDirectaAlmacenDetalle();
        logger.debug("## >> end: guardarCantidadEntradaSalidaDirectaAlmacenDetalleSeleccionado");
    }

    public void eliminarProducto(ActionEvent e) {
        FacesContext context = FacesContext.getCurrentInstance();
        Integer productoId = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("productoId"));

        logger.debug("## >> eliminarProducto: productoId=" + productoId);
        int indexToDelete = -1, i = 0;
        for (EntradaSalidaDirectaAlmacenDetalle dvp : entradaSalidaDirectaAlmacenDetalleList) {
            if (dvp.getProducto().getId().intValue() == productoId.intValue()) {
                indexToDelete = i;
                logger.debug("\t## >> indexToDelete=" + indexToDelete);
                break;
            }
            i++;
        }
        if (indexToDelete != -1) {
            EntradaSalidaDirectaAlmacenDetalle dvpDeleted = entradaSalidaDirectaAlmacenDetalleList.remove(indexToDelete);
            logger.debug("\t\t## >> dvpDeleted["+indexToDelete+"] = " + dvpDeleted);
        } else{
		    logger.debug("\t\t## >> can delete["+indexToDelete+"]");
			throw new IllegalStateException("can't delete row:"+indexToDelete);
		}
        logger.debug("## >> end: eliminarProducto");
    }
	private List<AlmacenProducto> getListAlmacenProductoBuscar(){
		if(listAlmacenProductoBuscar ==  null){
			Integer almacenId = getAlmacenObjetivo().getId();	
			logger.debug("## >> getListAlmacenProductoBuscar->getAlmacenObjetivo().getId()=" + almacenId);
			listAlmacenProductoBuscar = productoJPAController.findAllValidProductosForAlmacen(almacenId);			
			logger.debug("## >> getListAlmacenProductoBuscar->listAlmacenProductoBuscar.size()=" + listAlmacenProductoBuscar.size());			
		}
		return listAlmacenProductoBuscar;
	}
	
	public void cantidadDetalleBtnChanged(ActionEvent e) {
        FacesContext context = FacesContext.getCurrentInstance();
        Integer productoId = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("productoId"));
		
		logger.debug("## >> cantidadDetalleBtnChanged: productoId="+productoId);
		try{
			for (EntradaSalidaDirectaAlmacenDetalle dvp : entradaSalidaDirectaAlmacenDetalleList) {				
				logger.debug("## >> cantidadDetalleBtnChanged:\t"+dvp.getProducto().getCodigoBarras()+", "+dvp.getCantidad()+" ]");
				if(dvp.getProducto().getId().intValue() == productoId.intValue()){
					logger.debug("## >> cantidadDetalleBtnChanged:\t\tchanged this !");
				}
			}	
		}catch(Exception ex){
			FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al editar cantidad:", ex.getMessage()));
            
		}	
	}
	
	public void precioVentaDetalleBtnChanged(ActionEvent e) {
        FacesContext context = FacesContext.getCurrentInstance();
        Integer productoId = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("productoId"));
		
		logger.debug("## >> precioVentaDetalleBtnChanged: productoId="+productoId);
		try{
			for (EntradaSalidaDirectaAlmacenDetalle dvp : entradaSalidaDirectaAlmacenDetalleList) {
				logger.debug("## >> precioVentaDetalleBtnChanged:\t"+dvp.getProducto().getCodigoBarras()+", "+dvp.getPrecioVenta()+" ]");
				if(dvp.getProducto().getId().intValue() == productoId.intValue()){
					logger.debug("## >> precioVentaDetalleBtnChanged:\t\tchanged this !");
				}
			}	
		}catch(Exception ex){
			FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al editar cantidad:", ex.getMessage()));
            
		}	
	}
	
    public void nombreDescripcionChanged(ValueChangeEvent e) {
		String   nombrePresentacionBuscar  = ((String) e.getNewValue());
        String[] nombresPresentacionBuscar = nombrePresentacionBuscar.toLowerCase().split("([ ])+");
        logger.debug("## >> nombreDescripcionChanged: nombrePresentacionBuscar=" + nombrePresentacionBuscar+" =>"+Arrays.asList(nombresPresentacionBuscar));
        productoConNombreDescripcion = new ArrayList<SelectItem>();
		
		cantidadAgregar = null;		
		if (nombrePresentacionBuscar.trim().length() >= 3) {        
			String nombreDescripcionOriginal   = null;
			String nombreDescripcionOriginalLC = null;
			boolean found=false;
			for(AlmacenProducto ap: getListAlmacenProductoBuscar()){
				nombreDescripcionOriginal   = ap.getProducto().getNombre() + "/" + ap.getProducto().getPresentacion()+" ("+ap.getProducto().getContenido()+ap.getProducto().getUnidadMedida()+" / "+ap.getProducto().getUnidadesPorCaja()+"UxCj.) #"+ap.getCantidadActual();
				nombreDescripcionOriginalLC = nombreDescripcionOriginal.toLowerCase();
				found=false;
				for(String n:nombresPresentacionBuscar){
					if (nombreDescripcionOriginalLC.contains(n)) {
						found=true;				
					}
				}
				if(found){			
					cantidadAgregar = 1;
					productoConNombreDescripcion.add(new SelectItem(ap.getProducto().getId(), nombreDescripcionOriginal));
				}
			}
		}
    }
	private Producto productoSearchedAndSelected;
	
	public void cantidadAgregarChanged(ValueChangeEvent e) {
		Long   cantidadAgregarValue  = ((Long) e.getNewValue());
        logger.debug("## >> cantidadAgregarChanged: cantidadAgregarValue=" + cantidadAgregarValue);
		agregarProductoADetalle(productoSelected,cantidadAgregarValue.intValue());
        productoSelected = null;
        productoConNombreDescripcion = new ArrayList<SelectItem>();
        nombreDescripcion = null;
	}
	
	public void productoSelectedChanged(ValueChangeEvent e) {
        logger.debug("## >> productoSelectedChanged:" );		
	}

	
    public List<SelectItem> getProductoConNombreDescripcion() {
        return productoConNombreDescripcion;
    }
    
    //--------------------------------------------------------------------------
    public void setProductoJPAController(ProductoJPAController productoJPAController) {
        this.productoJPAController = productoJPAController;
    }

	public void setSucursalJPAController(SucursalJPAController sucursalJPAController) {
		this.sucursalJPAController = sucursalJPAController;
	}

	public void setEntradaSalidaDirectaBusinessLogic(EntradaSalidaDirectaBusinessLogic entradaSalidaDirectaBusinessLogic) {
		this.entradaSalidaDirectaBusinessLogic = entradaSalidaDirectaBusinessLogic;
	}
	
	
    public List<EntradaSalidaDirectaAlmacenDetalle> getEntradaSalidaDirectaAlmacenDetalleList() {
		logger.debug("## >> getEntradaSalidaDirectaAlmacenDetalleList:" );
		for(EntradaSalidaDirectaAlmacenDetalle esdd:entradaSalidaDirectaAlmacenDetalleList){
			logger.debug("## >> getEntradaSalidaDirectaAlmacenDetalleList:\t"+esdd.getCantidad()+" x "+esdd.getProducto().getCodigoBarras() );
		}
        return entradaSalidaDirectaAlmacenDetalleList;
    }

    public EntradaSalidaDirectaAlmacenDetalle getEntradaSalidaDirectaAlmacenDetalleSeleccionado() {
        return entradaSalidaDirectaAlmacenDetalleSeleccionado;
    }

    public void setEntradaSalidaDirectaAlmacenDetalleSeleccionado(EntradaSalidaDirectaAlmacenDetalle detalleVentaCompraSeleccionado) {
        this.entradaSalidaDirectaAlmacenDetalleSeleccionado = detalleVentaCompraSeleccionado;
    }

    public String getNombreDescripcion() {
        return nombreDescripcion;
    }

    public void setNombreDescripcion(String nombreDescripcion) {
        this.nombreDescripcion = nombreDescripcion;
    }

    public Integer getProductoSelected() {
        return productoSelected;
    }

    public void setProductoSelected(Integer productoSelected) {
        this.productoSelected = productoSelected;
    }

    /**
     * @return the productoJPAController
     */
    public ProductoJPAController getProductoJPAController() {
        return productoJPAController;
    }

    /**
     * @return the productoSearchedAndSelected
     */
    public Producto getProductoSearchedAndSelected() {
        return productoSearchedAndSelected;
    }

    /**
     * @param productoSearchedAndSelected the productoSearchedAndSelected to set
     */
    public void setProductoSearchedAndSelected(Producto productoSearchedAndSelected) {
        logger.debug(">> setProductoSearchedAndSelected: =" + productoSearchedAndSelected);
        this.productoSearchedAndSelected = productoSearchedAndSelected;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Integer getFormaDePagoId() {
        return formaDePagoId;
    }

    public void setFormaDePagoId(Integer formaDePagoId) {
        this.formaDePagoId = formaDePagoId;
    }

    public void setSessionUserMB(SessionUserMB sessionUserMB) {
        this.sessionUserMB = sessionUserMB;
    }
    
    //--------------------------------------------------------------------------

	/**
	 * @return the codigoBuscar
	 */
	public String getCodigoBuscar() {
		return codigoBuscar;
	}

	/**
	 * @param codigoBuscar the codigoBuscar to set
	 */
	public void setCodigoBuscar(String codigoBuscar) {
		this.codigoBuscar = codigoBuscar;
	}

	/**
	 * @return the almacenObjetivo
	 */
	public Almacen getAlmacenObjetivo() {
		if(almacenObjetivo==null){
			Sucursal sucursalPrincipal = sucursalJPAController.getSucursalPrincipal();

			Collection<Almacen> almacenCollection = sucursalPrincipal.getAlmacenCollection();
			for(Almacen a: almacenCollection){
				if(a.getTipoAlmacen() == modoVenta){
					almacenObjetivo = a;
					break;
				}
			}

		}
		return almacenObjetivo;
	}

	/**
	 * @param almacenObjetivo the almacenObjetivo to set
	 */
	public void setAlmacenObjetivo(Almacen almacenObjetivo) {
		this.almacenObjetivo = almacenObjetivo;
	}

	/**
	 * @return the modoVenta
	 */
	public int getModoVenta() {
		return modoVenta;
	}

	/**
	 * @param modoVenta the modoVenta to set
	 */
	public void setModoVenta(int modoVenta) {
		this.modoVenta = modoVenta;
	}

	/**
	 * @return the descuentoEspecial
	 */
	public int getDescuentoEspecial() {
		return descuentoEspecial;
	}

	/**
	 * @param descuentoEspecial the descuentoEspecial to set
	 */
	public void setDescuentoEspecial(int descuentoEspecial) {
		this.descuentoEspecial = descuentoEspecial;
	}

	private void actualizarAlmacenObjetivoDesdeModoVenta() {		
		logger.debug("## >> actualizarAlmacenObjetivoDesdeModoVenta: modoVenta="+modoVenta);
		
		//almacenObjetivo = null;
		if(sucursalPrincipal == null){
			sucursalPrincipal = sucursalJPAController.getSucursalPrincipal();
		}
		
		Collection<Almacen> almacenCollection = sucursalPrincipal.getAlmacenCollection();
		for(Almacen a: almacenCollection){
			if(a.getTipoAlmacen() == modoVenta){
				almacenObjetivo = a;
				logger.debug("-> actualizarAlmacenObjetivoDesdeModoVenta: sucursalPrincipal="+sucursalPrincipal.getId()+", almacenObjetivo="+almacenObjetivo.getId());
		
				break;
			}
		}
	}

	/**
	 * @return the cantidadAgregar
	 */
	public Integer getCantidadAgregar() {
		return cantidadAgregar;
	}

	/**
	 * @param cantidadAgregar the cantidadAgregar to set
	 */
	public void setCantidadAgregar(Integer cantidadAgregar) {
		this.cantidadAgregar = cantidadAgregar;
	}

}
